#!/bin/bash

echo "Starting Pwn containers"
pushd pwn/pwn_run_see/docker; ./start.sh; popd
pushd pwn/crazy_machine/docker; ./start.sh; popd
pushd pwn/javajail/docker; ./start.sh; popd
pushd pwn/bin_army_1/docker; ./start.sh; popd
pushd pwn/backdoor/docker; ./start.sh; popd

echo "Starting Crypto containers"
pushd crypto/secure_remote_password/docker; ./start.sh; popd
pushd crypto/black_box/docker; ./start.sh; popd

echo "Starting Misc containers"
pushd misc/x32_intro/docker; ./start.sh; popd
pushd misc/x32_advanced/docker; ./start.sh; popd
pushd misc/x32_emulator/docker; ./start.sh; popd
pushd misc/x32_assembler/docker; ./start.sh; popd
pushd misc/porte_blindee/docker; ./start.sh; popd
pushd misc/emoji/docker; ./start.sh; popd

echo "Starting Network containers"
pushd network/nothumans/docker; ./start.sh; popd

echo "Starting Web containers"
pushd web/what_the_auth/docker; ./start.sh; popd
pushd web/sometimes/docker; ./start.sh; popd
pushd web/hell_no_php/docker; ./start.sh; popd
pushd web/aperisolve_v0/docker; ./start.sh; popd
pushd web/tmnt/docker; ./start.sh; popd
pushd mobile/andrologger/docker; ./start.sh; popd
pushd web/js_art/docker; ./start.sh; popd
pushd web/js_injection/docker; ./start.sh; popd
pushd web/oracle/docker; ./start.sh; popd
pushd web/worldmeet/docker; ./start.sh; popd

echo "All containers should be up (please check!)"
screen -ls

echo "Running autoheal"
pushd infra/scripts/; screen -dmS heal python autoheal.py; popd
