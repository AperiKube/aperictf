#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# Author: Baptiste MOINE <contact@bmoine.fr>.

import argparse
import traceback
import zlib
import re
import os

from libs.shared import (calc_crc, change_height, logger)
from libs.png import (PNG, PNG_MAGIC_NUMBER, PNG_MAGIC_NUMBER_LENGTH)
from mmap import (ACCESS_READ, mmap)
from pyzbar import pyzbar
from PIL import Image

def lsb_decode(img):
    """Decode an LSB-encoded message."""
    data = ''.join([str(int(bit)%2) for bit in img.getdata()])

    return data

def decode_bits(bits, encoding='utf-8', errors='replace'):
    data = ''.join([chr(int(bits[i:i+8], 2)) for i in range(0, len(bits), 8)])

    return data

def parse_args():
    """Arguments parsing."""
    parser = argparse.ArgumentParser()

    parser.add_argument('-i', '--input',
                        type=str,
                        default='data/output.png',
                        help='path to the source image file')

    args = parser.parse_args()

    return args

def read_ztxt(img):
    """Read zTXt chunk of the image."""
    with open(img, 'rb') as fd:
        # Load the picture to memory.
        mm = mmap(fd.fileno(), 0, access=ACCESS_READ)

        # Skip file header.
        mm.seek(PNG_MAGIC_NUMBER_LENGTH, os.SEEK_SET)

        # Create the main IMAGE object.
        png = PNG(mm)

        # Hide data inside the zTXt chunk.
        png.readChunk()
        content = png.getChunk('zTXt').getBytes()
        ztxt_header = b'zTXt53cR37:\x00\x00'
        ztxt_offset = content.index(ztxt_header) + len(ztxt_header)
        content = zlib.decompress(content[ztxt_offset:]).decode('utf-8')

        # Free memory mapping.
        mm.close()

    return content

def main():
    """Load image and generate a candidate challenge file."""
    try:
        # Arguments parsing.
        args = parse_args()

        # Print configuration.
        logger.info('Input file: {0}'.format(args.input))

        # Load image.
        img = Image.open(args.input)

        # Get the flag.
        flag_re = r'(p[1-9]\:APRK{[^}]+})'

        # Fake size from IHDR.
        real_img_size = 822
        change_height(args.input, real_img_size)

        # Reload image.
        img = Image.open(args.input)

        # Decode QRCode.
        flag1 = pyzbar.decode(img)[0].data.decode('utf-8')

        # LSB decoding.
        flag2 = lsb_decode(img)
        flag2 = decode_bits(flag2)
        flag2 = re.search(flag_re, flag2).group(0)

        # zTXT chunk.
        flag3 = read_ztxt(args.input)

        # Palette hidden message.
        flag4 = ''.join(map(chr, img.getpalette())).replace('\x00', '')
        flag4 = re.search(flag_re, flag4).group(0)

        logger.debug('Flag 1: {0}'.format(flag1))
        logger.debug('Flag 2: {0}'.format(flag2))
        logger.debug('Flag 3: {0}'.format(flag3))
        logger.debug('Flag 4: {0}'.format(flag4))
    except (ValueError, TypeError, OSError, IOError):
        logger.error(traceback.format_exc())

## Runtime processor.
if __name__ == '__main__':
    main()
