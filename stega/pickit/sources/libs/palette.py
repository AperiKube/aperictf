#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# Author: Baptiste MOINE <contact@bmoine.fr>.

class Palette(object):
    """This class allows to format and manipulate color palette."""
    def __init__(self, palette=list()):
        """Constructor of I{Palette}.

        @param self: Current instance of I{Palette}.
        @param palette: Palette that we're working on.

        @type self: C{Palette}
        @type palette: C{list}

        @raise palette: TypeError, palette must be a valid C{list}.
        """
        if not isinstance(palette, list):
            raise TypeError('palette must be a valid list!')
        else:
            self.palette = palette

    def __getitem__(self, key):
        return self.palette[key]

    def __setitem__(self, key, item):
        self.palette[key] = item

    def __len__(self):
        return len(self.palette)

    def merge(self, palette):
        """Merge two palette."""
        palette_a = self.tuple
        palette_b = palette.tuple
        if len(palette_b) > len(palette_a):
            raise Exception('palette_a + palette_b must fit in the 256 colors!')
        else:
            for i in range(0, len(palette_b), 1):
                palette_a += [palette_b[i]]

        self.palette = self.untuple(palette_a)

    def fill(self, value=0):
        """Fill palette list."""
        palette = self.palette
        while len(palette) < 768:
            palette.append(value)
        self.palette = palette

    def pop_color(self, count=1):
        """Pop a color from palette list."""
        self.pop(count=count*3)

    def pop(self, count=1):
        """Pop values from palette list."""
        palette = self.palette
        for _ in range(count):
            palette.pop()
        self.palette = palette

    def append(self, values=(0, 0, 0)):
        """Append values to palette list."""
        palette = self.palette
        if len(palette) + len(values) <= 768:
            for value in values:
                palette.append(value)
        else:
            raise Exception('palette + values must fit in the 256 colors!')
        self.palette = palette

    def __str__(self):
        """Return human readable palette."""
        tuple_ = self.tuple

        palette_str = '['

        i = 0
        for color in tuple_:
            palette_str += '({0}, {1}, {2})'.format(str(color[0]).rjust(3, ' '),
                                                    str(color[1]).rjust(3, ' '),
                                                    str(color[2]).rjust(3, ' '))

            if i < len(tuple_) - 1:
                palette_str += ', '
                if (i + 1) % 8 == 0:
                    palette_str += '\n '

            i += 1

        palette_str += ']'

        return palette_str

    @property
    def tuple(self):
        """Create tupled palette ((r, g, b))."""
        tupled = []

        for i in range(0, len(self.palette), 3):
            c_1 = c_2 = c_3 = 0
            c_1 = self.palette[i+0]

            if len(self.palette) > i+1:
                c_2 = self.palette[i+1]

            if len(self.palette) > i+2:
                c_3 = self.palette[i+2]

            tupled += [(c_1,
                        c_2,
                        c_3)]

        self.__tupled = tupled

        return self.__tupled

    @property
    def raw(self):
        """Return raw palette."""
        raw = ((''.join(map(chr, self.palette))) + '\x00\x00\x00').ljust(768, '\xFF')

        self.__raw = raw

        return self.__raw

    @staticmethod
    def untuple(tupled):
        """Convert the tupled palette to a list."""
        untupled = []
        for color in tupled:
            for c_part in color:
                untupled += [c_part]
        return untupled

    def compress(self):
        """Remove the (0, 0, 0)s from the head of the list."""
        tuple_ = self.tuple
        palette = []
        stop = False

        for color in tuple_:
            if color != (0, 0, 0):
                stop = True
            
            if not stop:
                if color != (0, 0, 0):
                    palette.append(color)
            else:
                palette.append(color)

        self.palette = self.untuple(palette)

    def rotate(self):
        """Invert palette."""
        tuple_ = self.tuple
        self.palette = self.untuple(tuple_[::-1])
