#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# Author: Baptiste MOINE <contact@bmoine.fr>.

import logging
import binascii
import struct

## Logging configuration.
logger = logging.getLogger(__name__)    
handler = logging.StreamHandler()
handler.setFormatter(logging.Formatter('[%(levelname)s] %(message)s'))
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)

## IHDR manipulation.
CHUNK_SIZE_BYTES = 4
CHUNK_NAME_BYTES = 4
CHUNK_CRC_BYTES = 4

def calc_crc(img, ihdr_start):
    chunk = img[ihdr_start + CHUNK_SIZE_BYTES:
                ihdr_start + CHUNK_SIZE_BYTES + CHUNK_NAME_BYTES + 13]

    crc = binascii.crc32(chunk)

    return crc & 0xffffffff

def change_height(img, height):
    """Change height value in python IHDR chunk."""
    with open(img, 'rb+') as fd:
        data = bytearray(fd.read())
        get_height = lambda data, height_start: struct.unpack('>I', data[height_start:height_start + 4])[0]

        try:
            ihdr_start = data.index(b'IHDR') - CHUNK_SIZE_BYTES
            height_start = ihdr_start + CHUNK_SIZE_BYTES + CHUNK_NAME_BYTES + 4
            crc_start = ihdr_start + CHUNK_SIZE_BYTES + CHUNK_NAME_BYTES + 13
        except ValueError:
            raise Exception("The img file must be a valid PNG!")
        else:
            logger.info('Original height: {0}px'.format(get_height(data, height_start)))

            # Change size
            data[height_start:height_start + 4] = struct.pack('>I', height)

            # Fix CRC
            crc = calc_crc(data, ihdr_start)
            data[crc_start:crc_start + CHUNK_CRC_BYTES] = struct.pack('>I', crc)

            logger.info('New height: {0}px'.format(get_height(data, height_start)))

    with open(img, 'wb') as fd:
        fd.write(data)

