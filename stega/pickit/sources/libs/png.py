#!/usr/bin/env python3
# -*- coding:Utf-8 -*-

# Orginal author: Nathan RYDIN.
# Edited by: Baptiste MOINE <contact@bmoine.fr>.

import codecs
import binascii
import zlib
import struct

PNG_MAGIC_NUMBER = b'\x89PNG\r\n\x1a\n'
PNG_MAGIC_NUMBER_LENGTH = len(PNG_MAGIC_NUMBER)

class Chunk:
    def __init__(self, _type, data, crc):
        self.type = _type
        self.crc = crc
        self.raw = data

    def getBytes(self):
        _type = self.type

        crc = struct.pack('>I', binascii.crc32(_type + self.raw) & 0xffffffff)
        l = struct.pack('>I', len(self.raw))

        return l + _type + self.raw + crc

class PNG():
    def __init__(self, file):
        self.chunks = []
        self.file = file

        self.readChunk()

    def getChunk(self, wanted):
        for chunk in self.chunks:
            if chunk.type.decode('utf-8') == wanted:
                return chunk

    def readChunk(self):
        stop = False
        while not stop:
            length = self.file.read(4)

            if not length or len(length) == 0:
                stop = True
                break

            length = struct.unpack('>I', length)[0]
            _type = self.file.read(4)
            data = self.file.read(length)
            crc = self.file.read(4)

            current_chunk = Chunk(_type, data, crc)
            self.chunks.append(current_chunk)

    def hideData(self, data):
        new_data = zlib.compress(data.encode('utf-8'))
        new_chunk = Chunk(b'zTXt', b'53cR37:\x00\x00' + new_data, b'0000')
    
        self.chunks.insert(2, new_chunk)

    def getBytes(self):
        ret = b''
        for c in self.chunks:
            ret += c.getBytes()

        return ret
