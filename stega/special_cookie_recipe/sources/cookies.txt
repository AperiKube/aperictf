Recette cookies pour 20 cookies :

Ingredients :
-75g de sucre en poudre
-1 oeuf
-1 sachet de levure chimique
-125g de farine
-1/2 brique de creme fraiche epaisse
-100g de chocolat environ

Preparation :
1 : Verser dans un saladier : sucre, oeuf, creme
2 : Melanger
3 : Rajouter farine et levure
4 : Melanger
5 : Ajouter les pepites de chocolat et melanger
6 : Prechauffer le four a 180deg
7 : Disposer les cookies sur du papier sulfurise
8 : Laisser cuire environ 10 minutes
9 : Enjoy
