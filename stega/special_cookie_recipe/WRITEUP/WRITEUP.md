+++
title = "Special Cookie Recipe"
description = "Aperi'CTF 2019 - Steganography (??? pts)"
keywords = "Steganography, Casse, XSS, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = "2019-09-14T13:00:00+02:00"
weight = 20
draft = false
bref = "Aperi'CTF 2019 - Steganography (??? pts)"
toc = true
+++

Aperi'CTF 2019 - Special Cookie Recipe
============================================

## Challenge details

| Event          | Challenge             | Category      | Points | Solves |
|----------------|-----------------------|---------------|--------|--------|
| Aperi'CTF 2019 | Special Cookie Recipe | Steganography | ???    | ???    |

> Un ami m'a donné une recette pour faire des cookies, mais il me semble qu'il a quelque chose d'autre à me dire... Mais quoi ?
> Aide moi à retrouver son message s'il te plaît !

<u>**Fichier&nbsp;:**</u> [chall_cookies.txt](chall_cookies.txt) - md5sum: 44fdec242d0b4472fed42ebe89d45355


## TL;DR

Le message a été caché dans la casse. Il suffit de prendre un "0" pour chaque lettre minuscule, un "1" pour chaque lettre majuscule, et passer le tout en ascii ! :)


## Exploit

```bash
#!/usr/bin/env python2

with open("chall_cookies.txt", "r") as f:
    encoded = f.read()

flag_bin = str()
for e in encoded:
    if e in string.lowercase:
        flag_bin += "0"
    if e in string.uppercase:
        flag_bin += "1"
print flag_bin
chars = map(''.join, zip(*[iter(flag_bin)]*8))
flag = str()
for c in chars:
    flag += chr(int(c, 2))
print flag
```

Flag : `APRK{ciboulette}`


*Happy hacking !*

https://twitter.com/TheLaluka
