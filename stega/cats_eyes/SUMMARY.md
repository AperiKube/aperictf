# Cat's Eyes

Un laboratoire de recherche scientifique a subi une attaque informatique laissant fuiter quelques documents.

D'après les médias, ce laboratoire étudie, entre autres, les stimuli visuels et la persistance rétinienne chez les chats.

Vous avez récupéré l'un de ces documents, analysez-le pour en apprendre un peu plus sur l'objet de leurs recherches&nbsp;!

<u>**Fichier&nbsp;:**</u> [cats_eyes.png](files/cats_eyes.png) - md5sum: 273f3a2325172245ba78427d878bae1a
