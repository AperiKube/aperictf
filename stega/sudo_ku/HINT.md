# Hint for message size:
- Hint 1: Look at top left of the image.
- Hint 2: Message size is encoded in big endian.
- Hint 3: Message size is 444.

# Hint for data embedding:
- Hint 1: R = Y ; G = X (paper is wrong at some point)
- Hint 2: 123456789 (10) = 277266780 (9)
- Hint 3: "Hint" = 1214869108(10)

# Hint for sudoku:
- Hint 1: Gimp FTW
- Hint 2:
  - En: Filters > Distorsion > Whirlpinch
  - Fr: Filtres > Distorsion > Tourner et aspirer
- Hint 3: Many solutions ?
- Hint 4: [dcode.fr](https://www.dcode.fr)
