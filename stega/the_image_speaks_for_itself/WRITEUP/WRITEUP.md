+++
title = "The image speaks for itself"
description = "Aperi'CTF 2019 - Steganography (100 pts)"
keywords = "Corkami, Polyglot, BMP, WAV, Steg, Steganography, Stega, Audio, Sound, Header, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = "2019-09-14T13:00:00+02:00"
weight = 20
draft = false
bref = "Aperi'CTF 2019 - Steganography (100 pts)"
toc = true
+++

Aperi'CTF 2019 - The image speaks for itself
============================================

### Challenge details

| Event                    | Challenge                   | Category      | Points | Solves      |
|--------------------------|-----------------------------|---------------|--------|-------------|
| Aperi'CTF 2019           | The image speaks for itself | Steganography | 100    | ???         |

Trouvez le flag. Ici c'est relativement simple: l'image par d'elle même.

Challenge: [the_image_speaks_for_itself.bmp](the_image_speaks_for_itself.bmp) - md5sum: 52417e96e7ebadbe0959990f35d28714

### TL;DR

Open BMP as raw in Audacity an play the sound to have the flag.

### Methodology

#### Understand the hints

In this challenge we got two hints:
- The name of the challenge which says "speaks"
- The visual content of the BMP file which is a music controller.

The hints converge towards a sound file.

#### Play

First thing I did was trying to open the BMP file with Audacity (drag and drop). I got the following error:<br><br>
<center>![import_error.png](import_error.png)</center><br>

The error suggests to import the file as raw: "Importer Raw".

I imported the file as the following: File > Import > Import Raw

<center>![import_options.png](import_options.png)</center><br>

The text is a bit too fast, lets slow it down (Effects > Change speed):

<center>![speed.png](speed.png)<br>
<br>
![wave.png](wave.png)</center><br>

Let's play it:

```text
Bravo, le flag est APRK{BMP_PCM_POLYGLOT}, le tout en majuscule.
```

For more information, the challenge has been created using [this post](http://wiki.yobi.be/wiki/BMP_PCM_polyglot).

#### Flag

`APRK{BMP_PCM_POLYGLOT}`

[Zeecka](https://twitter.com/Zeecka_)
