# The image speaks for itself

Trouvez le flag. Ici c'est relativement simple: l'image parle d'elle-même.

<u>**Fichier&nbsp;:**</u> [the_image_speaks_for_itself.bmp](files/the_image_speaks_for_itself.bmp) - md5sum: 5ab5799e46c46b95e9428e6aa21518f1
