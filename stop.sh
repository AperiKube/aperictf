#!/bin/bash

echo "Killing autoheal"
screen -S heal -X quit

echo "Stopping Pwn containers"
pushd pwn/pwn_run_see/docker; docker-compose down; popd
pushd pwn/crazy_machine/docker; docker-compose down; popd
pushd pwn/javajail/docker; docker-compose down; popd
pushd pwn/bin_army_1/docker; docker-compose down; popd
pushd pwn/backdoor/docker; docker-compose down; popd

echo "Stopping Crypto containers"
pushd crypto/secure_remote_password/docker; docker-compose down; popd
pushd crypto/black_box/docker; docker-compose down; popd

echo "Stopping Misc containers"
pushd misc/x32_intro/docker; docker-compose down; popd
pushd misc/x32_advanced/docker; docker-compose down; popd
pushd misc/x32_emulator/docker; docker-compose down; popd
pushd misc/x32_assembler/docker; docker-compose down; popd
pushd misc/porte_blindee/docker; docker-compose down; popd
pushd misc/emoji/docker; docker-compose down; popd

echo "Stopping Network containers"
pushd network/nothumans/docker; docker-compose down; popd

echo "Starting Web containers"
pushd web/what_the_auth/docker; docker-compose down; popd
pushd web/sometimes/docker; docker-compose down; popd
pushd web/hell_no_php/docker; docker-compose down; popd
pushd web/aperisolve_v0/docker; docker-compose down; popd
pushd web/tmnt/docker; docker-compose down; popd
pushd mobile/andrologger/docker; docker-compose down; popd
pushd web/js_art/docker; docker-compose down; popd
pushd web/js_injection/docker; docker-compose down; popd
pushd web/oracle/docker; docker-compose down; popd
pushd web/worldmeet/docker; docker-compose down; popd

echo "All containers should be down (please check!)"
screen -ls
