#include "matrix.h"
#include <stdio.h>

matrix_t* createEmptyMatrix(int nrows, int ncols)
{
	matrix_t* matrix = (matrix_t*)malloc(sizeof(matrix_t) + (nrows * ncols) * sizeof(double));
	matrix->nrows = nrows;
	matrix->ncols = ncols;
	for (int i = 0; i < nrows * ncols; i++)
		matrix->data[i] = 0.0;
	return matrix;
}

matrix_t* createMatrix(double* data, int nrows, int ncols)
{
	matrix_t* matrix = (matrix_t*)malloc(sizeof(matrix_t) + (nrows * ncols) * sizeof(double));
	matrix->nrows = nrows;
	matrix->ncols = ncols;

	for (int i = 0; i < nrows * ncols; i++)
		matrix->data[i] = data[i];
	return matrix;
}

void displayMatrix(matrix_t* matrix)
{
	for (int i = 0; i < matrix->nrows; i++)
	{
		for(int j = 0; j < matrix->ncols; j++)
		{ 
			printf("%08.8lf, ", matrix->data[i * matrix->ncols + j]);
		}
		printf("\n");
	}
}
