import re
from z3 import *

DEBUG = False

def get_models(solver):
	models = []

	while solver.check() == sat:
	  model = solver.model()
	  models.append(model)
	  solver.add(Or([sym() != model[sym] for sym in model]))

	return models

with open('code.c', 'rt') as fd:
    lines = fd.read().split('\n')
    flag_varname = 'flag'
    i = 0

    while ('if' not in lines[i]):
        i += 1

    code_start = i

    while ('printf' not in lines[i]):
        i += 1

    code_end = i

    code = '\n'.join(lines[code_start:code_end])

    code = re.sub(r'([^\&])\n', r'\1', code, flags=re.MULTILINE)
    code = re.sub(r'(\&\&) ', r'\1\n', code, flags=re.MULTILINE)
    code = re.sub(r'^[\t ]*(.+)', r'\1', code, flags=re.MULTILINE)
    code = re.sub(r'\*%s' % (flag_varname), r'%s[0]' % (flag_varname), code, flags=re.MULTILINE)
    code = re.sub(r'(?:\((?:int)|(?:byte)|(?:char))\)', r'', code, flags=re.MULTILINE)
    code = re.sub(r'\'(\\?.)\'', lambda c: str(ord(eval("%s" % (c.group())))), code, flags=re.MULTILINE)
    code = re.sub(r'(0x[a-fA-F0-9]+)U', r'\1', code, flags=re.MULTILINE)
    code = re.sub(r'[\(\)]*', r'', code, flags=re.MULTILINE)
    code = re.sub(r'( *\&\& *)$', r'', code, flags=re.MULTILINE)
    code = re.sub(r',[\t ]*', r'\n', code)
    code = re.sub(r'^if ', r'', code)
    code = re.sub(r' {$', r'', code)
    if DEBUG: print(code)

    flag_indexes = (re.findall(r'%s\[((?:0x)?[0-9a-fA-F]+)\]' % (flag_varname), code, flags=re.MULTILINE))
    max_index = 0
    for index in flag_indexes:
        if '0x' in index:
            max_index = max(int(index[2:], 16), max_index)
        else:
            max_index = max(int(index, 10), max_index)

    flag = [BitVec('%i' % i, 8) for i in range(max_index + 1)]
    solver = Solver()

    for cond in code.split('\n'):
        if ('bVar' not in cond and not re.match('^[\t ]*$', cond) and flag_varname in cond):
            eval('solver.add(%s)' % (cond))

    # We know that the flag contains APRK{}
    eval('solver.add(%s[%d] == %d)' % (flag_varname, 0, ord('A')))
    eval('solver.add(%s[%d] == %d)' % (flag_varname, 1, ord('P')))
    eval('solver.add(%s[%d] == %d)' % (flag_varname, 2, ord('R')))
    eval('solver.add(%s[%d] == %d)' % (flag_varname, 3, ord('K')))
    eval('solver.add(%s[%d] == %d)' % (flag_varname, 4, ord('{')))
    eval('solver.add(%s[%d] == %d)' % (flag_varname, -1, ord('}')))

    # And that it's printable, but there's to many possibilites, we can try to guess the flag!
    for i in range(max_index + 1):
        eval('solver.add(%s[%d] >= 0x20)' % (flag_varname, i))
        eval('solver.add(%s[%d] <= 0x7e)' % (flag_varname, i))

    for model in get_models(solver):
        pwd = [''] * len(flag) # dummy list
        for name in model:
            index = int(name.__str__())
            char = chr(model[name].as_long())
            pwd[index] = char
        
        result = ''
        for index in range(len(pwd)):
            result += pwd[index] if pwd[index] else '*'

        print(result)