# Pet The Cat Writeup

Running ```file``` on the binary reveals it's a MachO 64-bit binary.

## Static analysis

Because not everybody has a Mac to run the program on, a static analysis can be performed on this binary.

Let's open IDA and start dissassembling.

![main1](images/main1.png)

This is the main function's start. Some gribberish can be seen at the start, it must be the flag in encrypted form.

After that a buffer is filled with null bytes and the function _pet()_ is called with 2 arguments :

1. argv[1]
2. The buffer full of nulls

![main2](images/main2.png)

Later in the same function, the size of the buffer is compared with the size of the encrypted flag and they must match. If it's the case and if the buffer is equal to the encrypted flag, the cat is pet. Let's take a look at what happens inside the previous function.

![pet1](images/pet1.png)

Inside the _pet()_ function the size of the first argument is checked before entering what seems to be a for-loop iterating over argv[1].

![pet2](images/pet2.png)

The for-loop takes 4 bytes from argv[1] and give them as parameter for the new function _dosomemagic()_. The result of this function is then copied in the second argument, the previous buffer.

There is only one last function to check.

![magic](images/magic.png)

This function looks very complicated but there is a strange constant 0xEDB88320. Because this seem to perform some cryptographic transformation, let's google this value to see if it correspond to any known algorithm.

By doing so, the function can be identified as a CRC32 checksum calculator.

To recap, the password is chunked in blocks of 4 letters and each chunk is replaced by it's CRC32 checksum.

## Decrypting the flag

Normally CRC32 is not reversible but because the input string is 4 bytes long, it can be done without brute-force.

The encrypted flag extracted from the binary is :

```Python
"\x3a\x69\xd3\x07\xe4\xe4\xfe\x88\x0c\xf8\xc8\x88\x81\xc3\xf7\x9d\x67\x40\x38\x9d\x2b\x1a\x18\x54\xf3\x58\xe6\xf0"
```

Because of the endianness, if we need to convert each chunk to a number, the first two would be :

```Python
0x07d3693a	# CRC32("APRK")
0x88fee4e4	# CRC32("{CRC")
```

After reversing this value and concatenating them we find the flag.

**APRK{CRC32_1s_r3v3rs1bl3_;)}**

Full script available [here](solve.py)

ENOENT
