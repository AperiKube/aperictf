# x32 Binary

Chez ENO.corp, ils sont tellement fiers de leur microcontrôleur qu'ils en ont mis partout !
Vous avez mis la main sur [le firmware](binary.bin) du microcontrôleur qui contrôle l'accès à la salle des serveurs.
Votre mission est de retrouver le mot de passe attendu par celui-ci et pénétrer dans cette salle !

<u>**Fichier&nbsp;:**</u> [binary.bin](files/binary.bin) - md5sum: a9452cffaeb90f0461158ab17e20c351
