# Secure Remote Password

Comme projet pendant ses cours de cryptographie votre fils a développé [un programme](files/srp.py) qui simule un échange du protocole SRP.
Cependant, il a tendance à se croire meilleur que les autres et se permet de modifier certaines choses pour des raisons de "performance".

En tant que cryptanalyste, vous lui avez fait remarquer que son implémentation est cassée dans le cas d'une attaque du type Homme du milieu, mais il ne vous croit pas.
Prouvez-lui qu'il a tord !

```bash
nc srp.aperictf.fr 9898
```

PS: Le brute-force du service n'est pas autorisé.

<u>**Fichier&nbsp;:**</u> [un programme](files/srp.py) - md5sum: b3725e4cce5aeec4071a5e92d88b862f
