#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# https://www.computest.nl/nl/knowledge-platform/blog/exploiting-two-buggy-srp-implementations/
from pwn import *
import itertools

def H(*args):  # a one-way hash function
    a = ':'.join(str(a) for a in args)
    return int(hashlib.sha256(a.encode('utf-8')).hexdigest(), 16)

N = 0xc037c37588b4329887e61c2da3324b1ba4b81a63f9748fed2d8a410c2fc21b1232f0d3bfa024276cfd88448197aae486a63bfca7b8bf7754dfb327c7201f6fd17fd7fd74158bd31ce772c9f5f8ab584548a99a759b5a2c0532162b7b6218e8f142bce2c30d7784689a483e095e701618437913a8c39c3dd0d4ca3c500b885fe3
g = 0x2
k = 0xb317ec553cb1a52201d79b7c12d4b665d0dc234fdbfd5a06894c1a194f818c4a

conn = remote("srp.aperictf.fr", 9898)
conn.recvuntil("1. client sends username I and public ephemeral value A to the server")
conn.recvuntil("I = ")
username = conn.recv(10)
conn.recvuntil("s = 0x")
salt = int(conn.recv(16), 16)
conn.recvuntil("B = 0x")
B = int(conn.recvline().strip(), 16)
partial_v = hex(B/k)[:60]
conn.recvuntil("What is the password ?")
found = ""
l = log.progress("Searching password...")
for p in itertools.product(string.ascii_lowercase + string.ascii_uppercase + string.digits, repeat=3):
    password = ''.join(p)
    x = H(salt, username, password)
    v = pow(g, x, N)
    if hex(v)[:60] == partial_v:
        found = password
        break
l.success("Found password : {}".format(found))
conn.sendline(found)
print(conn.recvlines(3)[2])
conn.close()
