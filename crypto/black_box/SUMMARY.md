# Black Box

La société ENO.corp organise un bug bounty pour mettre à l'épreuve la robustesse de leur tout nouveau HSM soi-disant ultra sécurisé. Vous n'avez pas accès physiquement au boitier, mais uniquement à un service de chiffrement.

```bash
nc black-box.aperictf.fr 9897
```
