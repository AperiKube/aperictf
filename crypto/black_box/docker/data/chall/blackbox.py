#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import binascii
import random
import sys

SEED = random.random()
N = 16*8

FLAG = b'APRK{Ev3ry_D4y_1_m_Shuffl1n}'

def ntos(x):
    n = hex(x)[2:].rstrip("L")
    if len(n)%2 != 0:
        n = "0"+n
    return binascii.unhexlify(n)

def ston(x):
    n = binascii.hexlify(x)
    return int(n,16)

def btos(b):
    n = int(b,2)
    return ntos(n)

def stob(s):
    return bin(ston(s))[2:].rstrip("L")

def get_blocks(data, size_block=16):
    return [data[i:i+size_block] for i in range(0, len(data), size_block)]

def enc(m):
    blocks = get_blocks(m)
    r = []
    for b in blocks:
        x = b.ljust(16, b"\x00")
        bi = list(stob(x).zfill(16*8))
        # print bi
        random.Random(SEED).shuffle(bi)
        # print bi
        r.append(btos(''.join(bi)).rjust(16, b"\x00"))
    r = b"".join(r)
    return r

print("Welcome to the bug bunty programm. A bounty will be awarded to those who can decrypt the following ciphertext :")
print('{}'.format(binascii.hexlify(enc(FLAG)).decode('utf-8')))
print("Good luck !")

i = 0
while i < N:
    print("Give me your plaintext:")
    plain = sys.stdin.readline().strip()
    try:
        data = binascii.unhexlify(plain)
    except:
        print("Please give me a valid hexadecimal string")
        sys.exit()
    print("Encrypted result:")
    print('{}'.format(binascii.hexlify(enc(data)).decode('utf-8')))
    i += 1
print("Maximum number of encryptions reached !")
