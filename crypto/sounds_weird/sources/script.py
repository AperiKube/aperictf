key = bytearray("Ap3r1kubefl4g!")

with open("flag.ogg", 'rb') as f:
    file1 = bytearray(f.read())

file2 = bytearray(len(file1))

for i in range(len(file1)):
    file2[i] = file1[i] ^ key[i%len(key)]

# Write the XORd bytes to the output file
with open("flag.ogg.crypt", 'wb') as f:
    f.write(file2)
