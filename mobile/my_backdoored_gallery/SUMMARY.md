# My Backdoored Gallery

Vous avez téléchargé une super application sur le PlayStore qui vous permet de visionner des photos de votre animal préféré.
Curieux, vous ouvrez logcat et voyez un message étrange.
Investiguez sur l'origine de ce message.

<u>**Fichier&nbsp;:**</u> [mygallery.apk](files/mygallery.apk) - md5sum: 0a9f7f8cefb2d0cde67ff66ff880b4a4
