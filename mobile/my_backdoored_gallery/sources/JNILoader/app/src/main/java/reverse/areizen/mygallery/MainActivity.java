package reverse.areizen.mygallery;

import android.Manifest;
import android.app.ActivityManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.io.File;
import java.io.InputStream;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    LinearLayout imageLayout;

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageLayout = (LinearLayout) findViewById(R.id.imageLayout);

        checkRoot();

        AssetManager aMngr = getAssets();
        loadImages(aMngr);
        loadGalleries(aMngr);
    }

    private static boolean checkRoot() {
        String[] paths = { "/system/app/Superuser.apk", "/sbin/su", "/system/bin/su", "/system/xbin/su", "/data/local/xbin/su", "/data/local/bin/su", "/system/sd/xbin/su",
                "/system/bin/failsafe/su", "/data/local/su", "/su/bin/su"};
        for (String path : paths) {
            if (new File(path).exists())android.os.Process.killProcess(android.os.Process.myPid());;
        }
        return false;
    }


    private void loadGalleries(AssetManager assetMgr) {
        try {
            final String[] imgPaths = assetMgr.list("");
            for (int i = 0; i < imgPaths.length; i++) {
                if (imgPaths[i].contains(".png")) {
                    InputStream is = null;
                    try {
                        is = assetMgr.open(imgPaths[i]);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    Bitmap image = null;
                    try {
                        image = BitmapFactory.decodeStream(is);

                        ImageView imgView = new ImageView(this);
                        imgView.setImageBitmap(image);

                        final int finalI = i;
                        imgView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(MainActivity.this, ImageViewActivity.class);
                                intent.putExtra("image", imgPaths[finalI]);
                                startActivity(intent);
                            }
                        });

                        imageLayout.addView(imgView);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native void loadImages(AssetManager mng);
}