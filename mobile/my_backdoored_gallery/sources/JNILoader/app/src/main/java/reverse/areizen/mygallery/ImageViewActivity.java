package reverse.areizen.mygallery;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import java.io.InputStream;

public class ImageViewActivity extends AppCompatActivity {

    ImageView imageFull;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_view);

        Bundle bundle = getIntent().getExtras();
        if(bundle != null && bundle.getString("image")!=null){

            AssetManager assetMgr = this.getAssets();
            InputStream is = null;
            try{
                is = assetMgr.open(bundle.getString("image"));
                Bitmap image = BitmapFactory.decodeStream(is);
                imageFull = (ImageView) findViewById(R.id.imageFull);
                imageFull.setImageBitmap(image);

            }catch (Exception e){
                e.printStackTrace();
            }
        }


    }

}
