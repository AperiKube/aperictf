#!/usr/bin/python2
#coding: utf8
from PIL import Image
import binascii
import base64
import sys

print(sys.argv[1])
lorem =""
f = base64.b64encode(open(sys.argv[1],"rb").read())
print(len(f))

for i in f:
	lorem +='{0:08b}'.format(i ^ 0x10)
img = Image.open(sys.argv[2]) # On ouvre l'image avec PIL
w,h = img.size
pxs = list(img.getdata())
newdata = []
l = len(lorem)
x = 0
for i in range(h):
    for j in range(0,w):
        c = pxs[i*w+j] # Current
        r = c[0] - c[0]%2 + int(lorem[ (x)  % l]) # On force LSB à 0 puis on ajoute
        g = c[1] - c[1]%2 + int(lorem[(x+1) % l]) # le bon LSB en fonction de
        #b = c[2] - c[2]%2 + int(lorem[(x+2) % l]) # string (lettre courrante)
        a = c[3] - c[3]%2 + int(lorem[(x+2) % l])
        newdata.append((r,g,c[2],a))
        x += 3

new = Image.new(img.mode,img.size)
new.putdata(newdata)

new.save(sys.argv[2] + "new" +".png")
