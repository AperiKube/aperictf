#!/usr/bin/python3
#coding: utf8
from PIL import Image
import binascii
import sys

img = Image.open(sys.argv[1]) # On ouvre l'image avec PIL
w,h = img.size
pxs = list(img.getdata())
newdata = ""
x = 0
for i in range(0,h):
    for j in range(0,w):
        c = pxs[i*w+j] # Current
        newdata += str(c[0]%2)
        newdata += str(c[1]%2)
        newdata += str(c[3]%2)
f = open(sys.argv[2],"wb")
#print(hex(int(newdata,2))[2:-1])
newdata = binascii.unhexlify(hex(int(newdata,2))[2:-1]+"0")

out=b""
for i in range(len(newdata)):
        out+=chr(newdata[i] ^ 0x10).encode('utf8')

f.write(out)