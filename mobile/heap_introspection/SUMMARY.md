# Heap Introspection

Votre collègue, développeur Android, a la fâcheuse habitude d'utiliser les mêmes identifiants en production et en phase de développement.

Depuis peu, sa marotte est d'optimiser la vitesse de ses applications. Il a donc réalisé des tests.

Vous avez trouvé deux fichiers sur le serveur de versioning interne à votre entreprise&nbsp;: [app-debug.apk](files/app-debug.apk) et [dump.hprof](files/dump.hprof).

Essayez de lui prouver que sa fâcheuse tendance est une mauvaise pratique.

<u>**Fichiers&nbsp;:**</u> 
- [app-debug.apk](files/app-debug.apk) - md5sum: cd4f3bbd17e3938040a4847d86b15287
- [dump.hprof](files/dump.hprof) - md5sum: 401c1d65d64025396b438aa23e92046d
