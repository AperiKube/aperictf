## Writeup Fuck_it

This application is obfuscated. The simpliest way I found to solve is dynamically :

script.js :

```javascript
// frida -U -l script.js -f com.example.challenge --no-pause
Java.perform(function(){
    const CryptoUtil = Java.use("com.example.challenge.CryptoUtil")

    CryptoUtil.af7bd616b.implementation = function(x){
        var out = this.af7bd616b(x)
        
        
        var buffer = Java.array('byte', out);
        console.log(buffer.length);
        var result = "";
        for(var i = 0; i < buffer.length; ++i){
            result+= (String.fromCharCode(buffer[i]));
        }
        console.log(result);

        return out
    }
})
```

## Flag
`APRK{ObfuscationIsCancer}`