$( document ).ready(function(){
    /**
    * Smooth scrolling to page anchor on click
    **/
    $("a[href*='#']:not([href='#'])").click(function() {
        if (
            location.hostname == this.hostname
            && this.pathname.replace(/^\//,"") == location.pathname.replace(/^\//,"")
        ) {
            var anchor = $(this.hash);
            anchor = anchor.length ? anchor : $("[name=" + this.hash.slice(1) +"]");
            if ( anchor.length ) {
                $("html, body").animate( { scrollTop: anchor.offset().top }, 1500);
            }
        }
    });
    $("#menumobile").click(function(){
            $("nav").fadeIn();
    });
    $("nav").click(function(){
        if ($(window).width() <= 600){
            $("nav").fadeOut();
        }
    });
    
    
    var countDownDate = new Date("Sep 14, 2019 18:00:00").getTime();
    var x = setInterval(function() {
      var now = new Date().getTime();
      var distance = countDownDate - now;
      var days = Math.floor(distance / (1000 * 60 * 60 * 24));
      var hours = days * 24 + Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((distance % (1000 * 60)) / 1000);
      $("#delay").text(hours.toString().padStart(2, '0') + " : "
      + minutes.toString().padStart(2, '0') + " : " + seconds.toString().padStart(2, '0'));

      // If the count down is finished, write some text
      if (distance < 0) {
        clearInterval(x);
        $("#delay").text("L'Aperi'CTF est terminé !");
      }
    }, 1000);
});

