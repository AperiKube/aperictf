#!/usr/bin/env python3
# -*- coding:utf-8 -*-
import socket
import sys

host = '239.255.255.250'
port = 1900

if len(sys.argv) > 1:
    service_type = sys.argv[1]
else:
    service_type = 'ssdp:all'

msg = '''M-SEARCH * HTTP/1.1
Host: %s:%d
Man: "ssdp:discover"
ST: %s
MX: 3
User-Agent: gssdp-discover GSSDP/1.0.1
'''.replace('\n', '\r\n') % (host, port, service_type)

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
s.settimeout(10)
s.sendto(msg.encode(), (host, port))

while True:
    try:
        datagram, address = s.recvfrom(32*1024)
    except socket.timeout:
        break
    else:
        print('=== Received from %s:%d ===\n\n%s=====================================\n' % (address[0], address[1], datagram))

