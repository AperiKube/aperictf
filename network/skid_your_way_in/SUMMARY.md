# SUMMARY

## Skid' your way in - 1

Énumérez le serveur cible, afin d'accéder à des données juteuses.

<u>URI&nbsp;:</u> aperivm.aperictf.fr

## Skid' your way in - 2

Un service requiert une authentification, réussirez à vous y connecter.

<u>**Note&nbsp;:**</u> Flag = APRK{user:pass}

## Skid' your way in - 3

Creusez du côté du service web.

## Skid' your way in - 4

Récupérez un shell sur le serveur avec l'utilisateur `francois`.

## Skid' your way in - 5

Élevez vos privilèges en utilisateur root.
