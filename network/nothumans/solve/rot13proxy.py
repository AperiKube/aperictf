#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Tcp Port Forwarding (Reverse Proxy)
# Author : WangYihang <wangyihanger@gmail.com>

import codecs
import socket
import threading
import sys

def handle(buffer):
    return codecs.encode(buffer.decode("utf-8"), 'rot_13').encode("utf-8")
    
def transfer(src, dst, direction):
    src_name = src.getsockname()
    src_address = src_name[0]
    src_port = src_name[1]
    dst_name = dst.getsockname()
    dst_address = dst_name[0]
    dst_port = dst_name[1]
    while True:
        buffer = src.recv(0x400)
        if len(buffer) == 0:
            break
        dst.send(handle(buffer))
    dst.shutdown(socket.SHUT_RDWR)
    dst.close()

def r13Thread(LHOST,LPORT,RHOST,RPORT):
    MAX_CONNECTION = 0x10
    
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind((LHOST, LPORT))
    server_socket.listen(MAX_CONNECTION)
    while True:
        local_socket, local_address = server_socket.accept()
        remote_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        remote_socket.connect((RHOST, RPORT))
        s = threading.Thread(target=transfer, args=(
            remote_socket, local_socket, False))
        r = threading.Thread(target=transfer, args=(
            local_socket, remote_socket, True))
        s.start()
        r.start()
        
    remote_socket.shutdown(socket.SHUT_RDWR)
    remote_socket.close()
    local_socket.shutdown(socket.SHUT_RDWR)
    local_socket.close()
    server_socket.shutdown(socket.SHUT_RDWR)
    server_socket.close()

if __name__ == "__main__":
	r13Thread("127.0.0.1",81,"humans.aperictf.fr",33331)
