-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mer. 24 avr. 2019 à 14:09
-- Version du serveur :  5.7.24
-- Version de PHP :  7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `s3cr3ts_us3rs`
--

-- --------------------------------------------------------


--
-- Structure de la table `s3cr3ts_us3rs`
--

DROP TABLE IF EXISTS `s3cr3ts_us3rs`;
CREATE TABLE IF NOT EXISTS `s3cr3ts_us3rs` (
  `nomutilisateur` text NOT NULL,
  `age` int(11) NOT NULL,
  `cipher_txt` text NOT NULL,
  `secret_key` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `s3cr3ts_us3rs`
--

INSERT INTO `s3cr3ts_us3rs` (`nomutilisateur`, `age`, `cipher_txt`, `secret_key`) VALUES
('test', 20, 'RZSkJgZi9hiZmEAxVJFIFg==', 'GhwrXEF50rquU4NpQZhrDw=='),
('Simon', 17, 'BU0Q9yto81kMIDM911kjuw==', 'JjAOMBl9lGpiqexsxQVu9A=='),
('Jean', 31, 'BFSAm13XYkNJV2yvnkQZ1g==', 'nmKzysnc6yur+Wwi0HKGSA=='),
('Adm1n1str4t0r', 999, 'yD0mgytKHeTh/h9lN5O8dKLetTKdORcPhQ9C7BOMUk0=', 'SzuYnjYuUkIupY/0nj84Qg==');



COMMIT;

GRANT SELECT on s3cr3ts_us3rs.* TO 'user' IDENTIFIED BY 'u4aQnuMh63EkmYT3AzGSVSVrcBZBs4CF';
GRANT FILE on *.* to 'user';

COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
