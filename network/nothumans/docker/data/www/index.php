<!doctype html>
<html>
<head>
    <meta charset="UTF-8" />
    <link rel="stylesheet" href="style.css"/>
    <title>Not Humans</title>
</head>
<body>
<!-- APRK{!S1MPL3_ROT13_R3QUEST!} -->
<div class="wrap" style="position: absolute;z-index:1000">
    <form id="search">
        <input type="text" id="searchTerm" class="searchTerm" placeholder="Qui cherchez-vous?">
        <button type="submit" class="searchButton"><img src="zoom.svg" width="20px" height="20px"/></button>
    </form>
    <div class="result"></div>
</div>
<div id="particles-js"></div>


<script src="js/particles.min.js"></script><!-- particles.js lib - https://github.com/VincentGarreau/particles.js -->
<script src="js/jquery-3.4.0.min.js"></script>
<script src="js/javascript.js"></script>
<script src="js/particlescontent.js"></script>
</body>
</html>
