+++
title = "Aperi'VP"
description = "Aperi'CTF 2019 - Forensic (100 pts)"
keywords = "Forensic, Wireshark, Binwalk, Videoprojector, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = "2019-09-14T13:00:00+02:00"
weight = 20
draft = false
bref = "Aperi'CTF 2019 - Forensic (100 pts)"
toc = true
+++

Aperi'CTF 2019 - Aperi'VP
============================================

### Challenge details

| Event                    | Challenge   | Category | Points | Solves      |
|--------------------------|-------------|----------|--------|-------------|
| Aperi'CTF 2019           | Aperi'VP    | Forensic | 100    | ???         |

### Description

Un boîtier d'interception Wi-Fi Pineapple a été retrouvé dans une salle de réunion dont l'accès était supposé sécurisé.

Soucieux des informations ayant pu être interceptées, vous avez été missionné par le directeur technique de votre entreprise pour identifier ces informations et lui renvoyer au plus vite.

Challenge: [chall.pcap](chall.pcap) - md5sum: 48de6bf8585dc8403cd98867bde175f4

### TL;DR

Export raw data with Wireshark, extract JPEG files, read the flag.

### Aperi'VP

We’re given a [chall.pcap](files/chall.pcap) PCAP file.

Task description:

 > A Wi-Fi Pineapple device has been found in a meeting room that was supposed to be safe.
 >
 > Concerned about the information that may have been captured, you have been asked by your company's CTO to identify this information and send it back to him as soon as possible.

### Methodology

#### PCAP analysis

Since, we're given a PCAP file, let's enumerate the protocols using `tshark`:

```bash
tshark -r files/chall.pcap -T fields -e frame.protocols | sort | uniq
```

```
eth:ethertype:ip:igmp:igmp
eth:ethertype:ip:tcp
eth:ethertype:ip:tcp:data
eth:ethertype:ip:tcp:data:vssmonitoring
eth:ethertype:ip:udp:data
```

Okay, except for multicast traffic, there's not much relevant information here.
Let's see which destination ports are being used (for service identification):

```bash
tshark -r files/chall.pcap -T fields -Y "udp or tcp" -e tcp.dstport -e udp.dstport | perl -p -e 's/[\t ]+//g' | sort | uniq
```

```
3620
3621
```

Looking at the [IANA port number assignment registry](https://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.txt), we can see that these ports are assigned to [Seiko Epson Corporation](https://en.wikipedia.org/wiki/Seiko_Epson):

| Service Name | Port Number | Transport | Description                  | Assignee        |
|:------------:|:-----------:|:---------:|:----------------------------:|:---------------:|
| `ep-pcp`     | `3620`      | `tcp`     | EPSON Projector Control Port |                 |
| `ep-pcp`     | `3620`      | `udp`     | EPSON Projector Control Port |                 |
| `ep-nsp`     | `3621`      | `tcp`     | EPSON Network Screen Port    | [SEIKO_EPSON_3] |
| `ep-nsp`     | `3621`      | `udp`     | EPSON Network Screen Port    | [SEIKO_EPSON_3] |

The EPSON documentation provide this information:

 > Port 3620 is necessary for searching for projectors.  If this port is blocked, projectors are notdetected.

We now know that the Wi-Fi Pineapple has captured a remote display session on an EPSON screen!

#### Exporting display session

Since EPSON doesn't provide much information about how the remote display session works, let's analyze the PCAP with Wireshark!

First, let's get rid of the control traffic:

```perl
not (tcp.port == 3620 || udp.port == 3620)
```

There is only one IGMP exchange and one TCP session on port 2021 left:

![epson_display_session](img/epson_display_session.png)

Looking at the stream, we quickly notice that there are JPEG magic numbers (`JFIF`), let's extract the TCP stream using either Wireshark (change `Show and save data as` from `ASCII` to `Raw` and save it to your hard drive) or `tshark`:

```bash
tshark -r files/chall.pcap -T fields -Y "tcp.port==3621" -e tcp.stream | sort -n | uniq  # get TCP session id
tshark -r files/chall.pcap -T fields -e data -qz follow,tcp,raw,12 | tail -n+7 | tr -d '=\r\n\t' | xxd -r -p >data.raw  # extract TCP stream data
```

Now, let's retrieve all the JPEG files froms the dump:

```bash
binwalk --dd='jpeg:jpg' -C img/ data.raw
```

Sample output:

![output_1990C0](img/1990C0.jpg)<!--
-->![output_19BF17](img/19BF17.jpg)<br style="line-height: 0"/>
![output_19F22F](img/19F22F.jpg)<!--
-->![output_1A10EC](img/1A10EC.jpg)

#### Flag

The final flag is `APRK{N3v3r_F0rgeT_€ncrypt10n!}`

*Happy Hacking!*

*Creased*
