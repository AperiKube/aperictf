# Aperi'VP

Un boîtier d'interception Wi-Fi Pineapple a été retrouvé dans une salle de réunion dont l'accès était supposé sécurisé.

Soucieux des informations ayant pu être interceptées, vous avez été missionné par le directeur technique de votre entreprise pour identifier ces informations et lui renvoyer au plus vite.

<u>**Fichier&nbsp;:**</u> [chall.pcap](files/chall.pcap) - md5sum: 48de6bf8585dc8403cd98867bde175f4
