import itertools
import string
import random
from Crypto.Cipher import XOR 

FLAG = '\x1d\xba\xe1\x89'

def sxor(key, data):
  return XOR.new(key).encrypt(data)

printable_set = set(string.ascii_letters + string.digits)
key = ''

while len(key) < len(FLAG):
    cand = random.choice(list(map(chr, range(48, 123))))
    result = sxor(FLAG[len(key)], cand)
    if set(result).issubset(printable_set):
        key += cand
        print('[...] key : %s' % repr(key))

print('Key: %s' % key)
print('Result: %s' % sxor(key, FLAG))
