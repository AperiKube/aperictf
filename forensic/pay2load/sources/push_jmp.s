/* gcc -m32 -z execstack -nostdlib push_jmp.s -o push_jmp */
/* objdump -s -j .text push_jmp */
.intel_syntax noprefix
.global _start
_start:
    push 0x80
    push 0xcd000000
    push 0x00bb0000
    push 0x0001b880
    push 0xcd000000
    push 0x1dbae189
    push 0x4b525041
    push 0x68315250
    push 0x7b686234
    push 0x376e6853
    push 0x5f336c68
    push 0x6c6c3368
    push 0x68656430
    push 0x63680a7d
    push 0x21216851
    push 0xc9310000
    push 0x0001bb00
    push 0x000004b8

    jmp esp
