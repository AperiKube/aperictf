/* gcc -m32 -z execstack -nostdlib code.s -o code */
/* objdump -s -j .text code */
.intel_syntax noprefix

.global _start

_start:
    /* push APRK{PR1n74bl4_Sh3llc0de!!} */
    mov eax, 0x4     /* write() */
    mov ebx, 0x1     /* STDOUT */
    xor ecx, ecx
    push ecx
    push 0x0a7d2121
    push 0x65643063
    push 0x6c6c3368
    push 0x535f336c
    push 0x6234376e
    push 0x3152507b
    push 0x4b525041
    mov ecx, esp     /* Get flag pointer */
    mov edx, 0x1D    /* size */
    int 0x80

    mov eax, 0x1     /* exit */
    mov ebx, 0x0
    int 0x80
