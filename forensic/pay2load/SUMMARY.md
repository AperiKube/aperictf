# Pay2Load

Notre entreprise a récemment été ciblée par une campagne de ransomware.

Malgré nos sondes d'analyse en profondeur de paquets, l'attaquant a réussi à s'infiltrer et à compromettre l'un de nos serveurs en exploitant une vulnérabilité du type 0day.

Fort heureusement, l'équipe forensique a réussi à récupérer la trace d'une charge utile suspectée avoir été utilisée au cours de cette attaque.

Analysez cette charge utile !

<u>**Note&nbsp;:**</u> Le serveur ciblé lors de cette attaque est basé sur un système Linux.

<u>**Fichier&nbsp;:**</u> [data.bin](files/data.bin) - md5sum: 74b458ee697d888aac299f2f74f4b5bb
