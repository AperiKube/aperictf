# Don't pay ransom

Un étudiant a été victime d'une attaque par ransomware et a perdu l'accès à son secret.

Venez-lui en aide et retrouvez son secret.

<u>**Fichier&nbsp;:**</u> [Aziram.github.io.crypted](files/Aziram.github.io.crypted) - md5sum: 715c6d9a185f7d6054b5dd7366bd2502
