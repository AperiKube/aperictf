# Pick up the phone - Partie 2

Suite à l'attaque au président à l'encontre du directeur commercial de notre entreprise, notre équipe d'ingénieurs a conçu un nouveau système d'authentification basé sur un jeton à usage unique (HOTP).

Afin de récupérer ce jeton, les utilisateurs peuvent appeler un service vocal dédié et récupérer un nouveau jeton sur leur afficheur.

Malgré les recommandations d'utilisation, certains utilisateurs ont tendance à créer une liste de jetons en appelant plusieurs fois le service vocal afin de les utiliser ultérieurement...

Notre système de communication VoIP semble cependant avoir été mis sur écoute et l'un de ses jetons encore inutilisés de notre directeur général a fuité.

Investiguez et retrouvez le jeton d'authentification ayant fuité afin de permettre à nos techniciens de le révoquer.

<u>**Fichier&nbsp;:**</u> [call.pcap](files/call.pcap) - md5sum: 185d1606b5aa637856e8216dc103fcd8
