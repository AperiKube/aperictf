#!/usr/bin/env python2
# -*- coding:utf-8 -*-

import random
import string
import hashlib

content = ''
with open('oprikube.csv', 'wt') as fd:
    content += '"Account","Login Name","Password","Web Site","Comments"\n'
    flag_pos = random.randint(0x100, 0xFF0)
    for i in range(random.randint(0xFF, 0xFFF)):
        header = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(4))
        if (i == flag_pos): header = 'APRK'
        content += '"user{0}","user{0}","{1}{{{2}}}","",""\n'.format(i, header, hashlib.sha1(str(i).encode('utf-8')).hexdigest())
    fd.write(content)
