# OP'Rikube

Nos services de renseignements se sont infiltrés sur le canal Telegram d'un groupe hacktiviste suspecté d'avoir participé à l'opération #OP'Rikube.

De nombreux messages ont été supprimés sur ce canal suite à l'exécution de l'opération. Cependant, un fichier a pu être récupéré.

Selon nos sources, de nombreux hackers ont participé à cette opération, l'un d'entre eux serait un membre du groupe Aperi'Kube. 

Analysez ce fichier et retrouvez des informations sur ce hacker.

<u>**Fichier&nbsp;:**</u> [oprikube.kdbx](files/oprikube.kdbx) - md5sum: 1eabca64cb4da20aa18f6a0e4e169987
