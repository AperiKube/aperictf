# Backdoor-333

Reynholm Industries a de nouveau été victime d'une attaque sur l'un de leurs serveurs.

Suite la compromission du système et des identifiants d'un utilisateur du système, l'attaquant
semble avoir ajouté une backdoor lui permettant de récupérer un accès au compte root depuis
cet utilisateur.

Trouvez et analysez cette backdoor&nbsp;!

<u>Accès au serveur&nbsp;:</u>

```bash
sshpass -p WRcPLQHRxoX6GWS7 ssh -p 31341 -o StrictHostKeyChecking=no chall@backdoor.aperictf.fr
```

Ou en ssh avec le mot de passe `WRcPLQHRxoX6GWS7`:
```bash
ssh -p 31341 -o StrictHostKeyChecking=no chall@backdoor.aperictf.fr
```

<u>**Fichier&nbsp;:**</u> [vmlinux](files/vmlinux) - md5sum: 6412ea7e27b1cec636c46590238ee8c0
