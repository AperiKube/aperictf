#!/bin/bash -p
# Inspired by root-me starting script.

PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
HOMEPATH=/root/qemu
 
STTY=$(stty -g)  # save terminal line settings.
stty intr ^-  # unmap ^c.

# create a shared folder with the vm.
TEMP=$(mktemp -d)
chgrp chall ${TEMP}
chmod 770 ${TEMP}

cat <<-EOF

A share will be available: host:${TEMP} -> guest:/mnt/share
Launching the vulnerable machine...

EOF

qemu-system-x86_64 \
    -m 128M \
    -kernel ${HOMEPATH}/kernel \
    -initrd ${HOMEPATH}/initrd \
    -nographic \
    -monitor /dev/null \
    -snapshot \
    -virtfs local,id=host0,path=${TEMP},security_model=mapped,mount_tag=host0 \
    -append "nokaslr root=/dev/ram rw console=ttyS0 oops=panic panic=1 quiet"

rm -rf "${TEMP}" 2>/dev/null
stty "${STTY}"  # restore terminal settings.