from scapy.all import *

p = rdpcap("dump.pcap")
output = ''
for i in range(len(p)-1):
	if (p[i+1].time - p[i].time) > 3:  # Plus de 3 secondes (SLEEP 3)
		rep = str(p[i][Raw])
		rep = rep.split("%20AND%20SLEEP(3)")[0].split("=")[-1]
		rep = int(rep)
		if rep != 0:
			output += chr(rep)
		elif output[-1] != '\n':
			output += '\n'
print(output)  # APRK{SqLi_LoGs_ArE_s0_b1g_x1y2z3}
