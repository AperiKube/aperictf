# Web logs

Le site web de l'entreprise Foo-Bar s'est fait attaquer la semaine dernière. L'administrateur a eu le temps de lancer une capture Wireshark afin d'avoir une traçabilité des potentielles fuites de données. Investiguez et retrouvez les données exfiltrées.

<u>**Fichier&nbsp;:**</u> [dump.pcap](files/dump.pcap) - md5sum: c6934a72b6db09d78baf72748c6cd4a4
