# Pwn-Run-See writeup

TL;DR: the challenge is based on a `use-after-free` vulnerability enabling to get a shell and escape the docker
container using a race condition on `runC`.

Detailed writeups: [part1](WRITEUP_PART1.md) and [part2](WRITEUP_PART2.md)

Exploitation&nbsp;:

```bash
pushd exploit/
python exploit.py
```

Flag 1&nbsp;: `APRK{Us3_3m_4Ll_4f73r_fR3e!}`
Flag 2&nbsp;: `APRK{N3V3r_l0ok_b4cK_4Nd_w1n_thE_RAc3!}`
