# Pwn-Run-See - Installation

Installation de `docker-ce` dans sa version `18.09.1`&nbsp;:

```bash
apt install -o Acquire::ForceIPv4=true libltdl7
git clone https://gist.github.com/Creased/d4c493cac872ff373f9c05c8e7d0f839 pkg
pushd pkg/
dpkg -i *.deb
popd
rm -rf pkg/
```

Installation de docker-compose&nbsp;:

```bash
curl --location --url "https://github.com/docker/compose/releases/download/$(curl --head --silent --url 'https://github.com/docker/compose/releases/latest' | grep -oP '^(?:Location).+$' | awk -F'/' '{print $(NF)}' | grep -oP '([0-9]\.?){2,}')/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
docker-compose version
```