# Crazy Machine

Reynholm Industries est une entreprise mystérieuse qui ne souhaite pas révéler son activité au public.

Cependant, suite à une investigation, vous avez découvert un service étrange exposé sur l'un de leurs serveurs.

Étudiez son fonctionnement et récupérez des informations sur Reynholm Industries.

```bash
nc crazy-machine.aperictf.fr 31338
```

<u>**Fichiers&nbsp;:**</u>
- [chall](files/chall) - md5sum: aaee80780ac6e9f25d9134ffa20f21b2
- [chall.c](sources/chall.c) - md5sum: 25f62042b03517c7f158231138ee5168
- [libc.so.6](sources/libc.so.6) - md5sum: b4e619245a924b949d8ba780e1a07cee
