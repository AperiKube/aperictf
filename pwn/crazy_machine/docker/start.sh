#!/bin/bash
chown 0:0 data/chall/chall
chown 0:0 data/chall/flag
chmod 4550 data/chall/chall
chmod 400 data/chall/flag

docker-compose pull
docker-compose build
docker-compose push
docker-compose up -d

source .env
screen -dmS ${COMPOSE_PROJECT_NAME} docker-compose logs -f

