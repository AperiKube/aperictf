# Bin Army 1

SecureByDesign Corp propose un moyen révolutionnaire permettant de limiter la contagion des virus utilisant des buffers overflow pour se répandre. La solution était pourtant simple : utiliser des buffers ayant des tailles variables !

> "Bah oui, de cette manière, un exploit ne marchera jamais partout, c'est pourtant simple !" - Jean Michel Crédible, CEO de SecureByDesign Corp

Chaque fichier sensible est caché par un binaire différent, à vous de leur prouver que leur solution ne vaut pas la peau d'un smourbiff !

<u>Accès au serveur&nbsp;:</u>

```bash
sshpass -p 7heqDtFq6b83qx9s ssh -p 31340 -o StrictHostKeyChecking=no chall@binarmy1.aperictf.fr
```

Ou en ssh avec le mot de passe `7heqDtFq6b83qx9s`:
```bash
ssh -p 31340 -o StrictHostKeyChecking=no chall@binarmy1.aperictf.fr
```
