#!/usr/bin/env python2
# -*- coding:utf-8 -*-
# ./main.py clean; ./main.py setup; ./main.py test

from os import popen, system
from random import randint
import sys

def disp_help():
    print("Usage: {:s} setup | clean | test".format(sys.argv[0]))
    exit(1)

if len(sys.argv) != 2:
    disp_help()

# consts
action = sys.argv[1]
FLAG = "APRK{it_takes_an_army}"
NB_CHALLS = len(FLAG)

if action == "setup":
    with open("template.c", "r") as f:
        template = f.read()

    system("mkdir -p bin")
    system("mkdir -p flag")
    system("mkdir -p src")

    for cpt in range(NB_CHALLS):
        name = "bof_{:02d}".format(cpt)
        user = "user_binarmy1_{:02d}".format(cpt)
        elf_code = template.replace("RANDOM", str(randint(100, 1000)))
        src_name = "src/{:s}.c".format(name)
        elf_name = "bin/{:s}".format(name)
        flag_char_file = "flag/{:02d}".format(cpt)

        with open(src_name, "w") as f:
            f.write(elf_code)

        cmd_gcc = "gcc {:s} -fno-stack-protector -no-pie -m32 -o {:s}".format(src_name, elf_name)
        print("cmd_gcc:", cmd_gcc)
        system(cmd_gcc)

        with open(flag_char_file, "w") as f:
            f.write(FLAG[cpt])

        system("useradd -M --shell /bin/false {:s}".format(user))
        system("passwd -d {:s}".format(user))
        system("chown {:s}:{:s} {:s} {:s}".format(user, user, elf_name, flag_char_file))
        system("chmod 555 {:s}".format(elf_name))
        system("chmod a+s {:s}".format(elf_name))
        system("chmod 400 {:s}".format(flag_char_file))
    print("setup done.")
elif action == "clean":
    system("/bin/rm -rf src bin flag")
    for cpt in range(NB_CHALLS):
        user = "user_binarmy1_{:02d}".format(cpt)
        system("userdel {:s}".format(user))
    print("clean done.")
elif action == "test":
    flag = popen("find flag | sort | xargs cat 2>/dev/null").read()
    print("Flag found: {:s}".format(flag))
    if (FLAG in flag):
        print("Files setup done")
        print("If the flag found is ccccccccc...")
        print("Replace /bin/sh (link to /bin/bash) by dash")
        system("./exploit.py")
    else:
        print("Files setup failed")
    print("Test done.")
else:
    disp_help()
