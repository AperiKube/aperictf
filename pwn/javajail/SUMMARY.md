### Java-jail

Dans le cadre du mouvement #BalanceTaCVE, la société "Un PoC, un cookie" met à disposition un environnement vulnérable . Saurez-vous sortir de la JVM ?

<u>**Note&nbsp;:**</u> Le flag est dans le fichier /passwd

<u>Accès au serveur&nbsp;:</u>

```bash
sshpass -p V5fCqvaGULh7XGK3 ssh -p 31339 -o StrictHostKeyChecking=no chall@javajail.aperictf.fr
```

Ou en ssh avec le mot de passe `V5fCqvaGULh7XGK3`:
```bash
ssh -p 31339 -o StrictHostKeyChecking=no chall@javajail.aperictf.fr
```
