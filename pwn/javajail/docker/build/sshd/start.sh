#!/bin/bash

rm /etc/ssh/ssh_host_*key* && \
ssh-keygen -N '' -t ed25519 -f /etc/ssh/ssh_host_ed25519_key </dev/null && \
ssh-keygen -N '' -t rsa -b 4096 -f /etc/ssh/ssh_host_rsa_key </dev/null && \
ssh-keygen -N '' -t ecdsa -f /etc/ssh/ssh_host_ecdsa_key </dev/null

/etc/init.d/ssh start
sleep infinity
