+++
title = "Pwn The Scam 4 - Archived"
description = "Aperi'CTF 2019 - OSINT (175 pts)"
keywords = "OSINT, Wayback, Cache, Old, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = "2019-09-14T13:00:00+02:00"
weight = 20
draft = false
bref = "Aperi'CTF 2019 - OSINT (175 pts)"
toc = true
+++

Aperi'CTF 2019 - Pwn The Scam 4 - Archived
============================================

### Challenge details

| Event                    | Challenge                 | Category | Points | Solves      |
|--------------------------|---------------------------|----------|--------|-------------|
| Aperi'CTF 2019           | Pwn The Scam 4 - Archived | OSINT    | 175    | ???         |

Un [site de scam Bitcoin](http://ylsspycahtqrv3u2.onion) a été découvert sur TOR. Vous avez été missionné pour en prendre le contrôle.

> *Pwn The Scam* est un challenge d'OSINT, il n'y a pas de vulnerabilité web à exploiter!
> Format de flag : *APRK{flag}*.

Identifiez le scammeur.

### TL;DR
Use [Wayback Machine](https://web.archive.org) to get the flag and the twitter of the scammer.

### Methodology

Looking at the challenge name, we may need a cache/old copy of the website to browse older information. We could have use google cache but there is no information about the website.

<center>![cache.jpeg](cache.jpeg)</center>
<br>
There is a famous website called [Wayback Machine](https://web.archive.org) which is usually use to keep a snapshot of a website for a given time. Lets search on it.

We got many snapshots, if we take the one for 03 JUL 2018 we got:
<br><br>[https://web.archive.org/web/20180703085735/http://nothing-here.com/]
<center>![wayback.jpeg](wayback.jpeg)</center>
<br>
[https://web.archive.org/web/20180703085735/http://nothing-here.com/](https://web.archive.org/web/20180703085735/http://nothing-here.com/)

We get the flag but we also get a twitter profile "[By 751](https://twitter.com/bytet0r)".

#### Flag

`APRK{B4ck_1N_T1m3!}`

Challenge by [DrStache](https://twitter.com/drstache_) , WriteUp by [Zeecka](https://twitter.com/Zeecka_)
