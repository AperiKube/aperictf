# Pwn The Scam - Archived

Un [site de scam Bitcoin](http://ylsspycahtqrv3u2.onion) a été découvert sur TOR. Vous avez été missionné pour en prendre le contrôle.

<u>**Notes&nbsp;:**</u> 
- *Pwn The Scam* est un challenge d'OSINT, il n'y a pas de vulnerabilité web à exploiter!
- Format de flag : *APRK{flag}*.

Identifiez le scammeur.
