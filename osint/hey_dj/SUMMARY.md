# Hey DJ

Votre ami vous assure que sa compositrice préférée (amatrice) `Twisore` garde son identité secrète. Prouvez-lui le contraire en investiguant.
Le flag est sous la forme `APRK{SHA1(NOMPRENOM)}`.

Par exemple, si l'artiste s'appelle `Foo BAR`, alors le flag serait `APRK{f100629727ce6a2c99c4bb9d6992e6275983dc7f}`.

<u>**Notes&nbsp;:**</u> 
- **Le bruteforce est inutile et interdit** sur ce challenge.
- L'identité de la compositrice est fictive et a été créée pour les besoins du challenge.
- Il s'agit d'un challenge **réaliste**.
- Une partie de la résolution du challenge implique une étape relativement "intrusive". En cas de doute, contactez un admin.

<u>**Credits&nbsp;:**</u> Compositeur original des morceaux : [LIND](https://soundcloud.com/reallind) (ne fait pas partie du challenge)
