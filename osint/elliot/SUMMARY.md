# Elliot

Un zip chiffré a été volé au personnage fictif "Elliot Alderson" de la série TV "Mr. Robot". 
Ce dernier utiliserait des mots de passes inférieurs à 30 caractères basés sur des informations personnelles et publiques.

Afin d'ouvrir son fichier zip, vous effectuerez une étape d'OSINT dans le but de générer une wordlist.

<u>**Fichier&nbsp;:**</u> [Private_Data.zip](files/Private_Data.zip) - md5sum: ec67cc5e6bf208d4070e848bbf459124
