# Pwn The Scam - Find Me

Un [site de scam Bitcoin](http://ylsspycahtqrv3u2.onion) a été découvert sur TOR. Vous avez été missionné pour en prendre le contrôle.

Investiguez sur ce que vous avez à disposition, chaque élément peut être déterminant.

<u>**Notes&nbsp;:**</u> 
- *Pwn The Scam* est un challenge d'OSINT, il n'y a pas de vulnerabilité web à exploiter !
