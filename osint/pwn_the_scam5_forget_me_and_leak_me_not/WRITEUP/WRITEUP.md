+++
title = "Pwn The Scam 5 - Forget me and leak me not"
description = "Aperi'CTF 2019 - OSINT (250 pts)"
keywords = "OSINT, Passwords, Reset, Twitter, Facebook, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = "2019-09-14T13:00:00+02:00"
weight = 20
draft = false
bref = "Aperi'CTF 2019 - OSINT (250 pts)"
toc = true
+++

Aperi'CTF 2019 - Pwn The Scam 5 - Forget me and leak me not
============================================

### Challenge details

| Event                    | Challenge                                  | Category | Points | Solves      |
|--------------------------|--------------------------------------------|----------|--------|-------------|
| Aperi'CTF 2019           | Pwn The Scam 5 - Forget me and leak me not | OSINT    | 250    | ???         |

Un [site de scam Bitcoin](http://ylsspycahtqrv3u2.onion) a été découvert sur TOR. Vous avez été missionné pour en prendre le contrôle.

> *Pwn The Scam* est un challenge d'OSINT, il n'y a pas de vulnerabilité web à exploiter!
> Format de flag : *APRK{flag}*.

Récoltez des informations sur le scammeur afin de vous introduire sur le site de scam.

### TL;DR
Twitter password reset, twitter screenshot about facebook, facebook password reset, email guessing, pastebin leak, log on admin pannel on TOR.

### Methodology

#### Get user email

Now we have a link thanks to Wayback Machine : "[By 751](https://twitter.com/bytet0r)" ( https://twitter.com/bytet0r ).
<center>![twitter.jpeg](twitter.jpeg)</center>
<br><br>
Let's try to reset the password to get a partial mail:
<br><br>
<center>![twitterreset.jpeg](twitterreset.jpeg)</center>
<br><br>
We get the partial mail: `t0********@g****.***`.<br>
We also have a screenshot:<br><br>
<center>![fbscreen.jpeg](fbscreen.jpeg)</center>
<br><br>

We can recognize the domain nothing-here.com and a Facebook account in the background: "Tor Byte".
A quick search led us to [https://www.facebook.com/tor.byte](https://www.facebook.com/tor.byte).

The account has no real information, no pictures, no post, no friends... Let's reset his password to complete our partial mail ! For this, go to forgot password, enter `tor.byte` in input field and submit (it correspond to the username in URL).
<br><br>
<center>![fbreset.jpeg](fbreset.jpeg)</center>
<br><br>
We can guess the email t0rbyte751@gmail.com. To verify, let's reset the facebook password with this email. The email is correct since we got a validation page.

#### Get user password

Now that we have the email, let's search for password leaks. Usually leaks may appear in big databases or in website like pastebin. A quick search on google with this email gave us only one link: `https://pastebin.com/pAe5JgcQ`.
<br><br>
<center>![pastebin.jpeg](pastebin.jpeg)</center>
<br><br>

We got a password for the mail account: `Z>ZYc-4[^JG3k6br`.
After few try on Google, Facebook, Twitter, the password doesn't seem to work... But what about TOR service ? If you go back to [http://ylsspycahtqrv3u2.onion/free-btc/admin](http://ylsspycahtqrv3u2.onion/free-btc/admin) and put the password `Z>ZYc-4[^JG3k6br`.
<br><br>
<center>![flag.jpeg](flag.jpeg)</center>
<br><br>

#### Flag

`APRK{P4stB1n_F0r_tH3_w1N}`

Challenge by [DrStache](https://twitter.com/drstache_) , WriteUp by [Zeecka](https://twitter.com/Zeecka_)
