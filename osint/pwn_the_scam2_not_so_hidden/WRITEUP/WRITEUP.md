+++
title = "Pwn The Scam 2 - Not so hidden"
description = "Aperi'CTF 2019 - OSINT (175 pts)"
keywords = "OSINT, TOR, Directory Listing, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = "2019-09-14T13:00:00+02:00"
weight = 20
draft = false
bref = "Aperi'CTF 2019 - OSINT (175 pts)"
toc = true
+++

Aperi'CTF 2019 - Pwn The Scam 2 - Not so hidden
============================================

### Challenge details

| Event                    | Challenge                      | Category | Points | Solves      |
|--------------------------|--------------------------------|----------|--------|-------------|
| Aperi'CTF 2019           | Pwn The Scam 2 - Not so hidden | OSINT    | 175    | ???         |

Un [site de scam Bitcoin](http://ylsspycahtqrv3u2.onion) a été découvert sur TOR. Vous avez été missionné pour en prendre le contrôle.

> *Pwn The Scam* est un challenge d'OSINT, il n'y a pas de vulnerabilité web à exploiter!
> Format de flag : *APRK{flag}*.

Vos investigations sont limitées au réseau TOR, faîtes en sorte d'étendre votre champ de recherche en identifiant l'adresse IP du serveur.
(flag : *APRK{IP}*)

### TL;DR
Shodan search with a specific header on the web server.

### Methodology

#### Identification

If we search with  differents keywords on the website such as the Bitcoin key on differents web search engine, we do not get any response. We need to look for an other hint which can lead us to the correct server. Let's look at the headers, sometimes headers may have information such as location, hostnames, IP addresses...

<center>![headers.jpeg](headers.jpeg)</center>
<br>
Here we got the Header `Via: httproxy`.

This header is not very common. Let's search this header on [`Shodan.io`](https://shodan.io/).

<center>![httproxy.jpeg](httproxy.jpeg)</center>

We got one result with the following @IP: `137.74.112.46`.
#### Flag

`APRK{137.74.112.46}`

Challenge by [DrStache](https://twitter.com/drstache_) , WriteUp by [Zeecka](https://twitter.com/Zeecka_)
