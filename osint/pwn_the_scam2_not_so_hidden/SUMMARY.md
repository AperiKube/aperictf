# Pwn The Scam - Not so hidden

Un [site de scam Bitcoin](http://ylsspycahtqrv3u2.onion) a été découvert sur TOR. Vous avez été missionné pour en prendre le contrôle.

<u>**Note&nbsp;:**</u> *Pwn The Scam* est un challenge d'OSINT, il n'y a pas de vulnerabilité web à exploiter!

Vos investigations sont limitées au réseau TOR, faîtes en sorte d'étendre votre champ de recherche en identifiant l'adresse IP du serveur. (flag : *APRK{IP}*)
