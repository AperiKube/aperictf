# Hell no PHP

Nous avons retrouvé un fichier "Lo7ef4fBi92x3p/index.php" sur le site web d'un client.
D'après nos informations, il s'agirait d'une backdoor permettant d'accéder au fichier `flag.php`.
Investiguez et trouvez comment cette backdoor peut être exploitée.

<u>URI&nbsp;:</u> [https://hell-no-php.aperictf.fr](https://hell-no-php.aperictf.fr)
