function processForm(e) {
    if (e.preventDefault) e.preventDefault();
    
    var current_note = {
        user: document.getElementById("user-input").value,
        message: document.getElementById("message-input").value
    }

    fetch("/createNote",
    {
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
        },
        method: "POST",
        body: JSON.stringify(current_note)
    })
    
    getNotes()
    return false;
}

function getNotes(){
    fetch("/getNotes").then(response => {
        return response.json()
    }).then(object => {
       
        html = ""

        for (var key in object) {
            if (!object.hasOwnProperty(key)) continue;

            html += "<h3>" + key + "</h3>"
            var obj = object[key];
            
            for(var i = 0; i<obj.length; i++){
                date  = new Date(obj[i].date)
                html += "<div><p>" + obj[i].message + " - " + date.getHours()  + " : " + date.getMinutes() + "</p></div>"
            }
        }
        console.log(html)
        document.getElementById("notes").innerHTML = html
    })

}
