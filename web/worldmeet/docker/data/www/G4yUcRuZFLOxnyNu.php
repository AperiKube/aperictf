<?php

// FLAG 3: APRK{H1DD3N_IN_0P_PR3L04D}

function get_flag_2(){
    return "FLAG 2: APRK{Sh4-SQLi-Little-BF}";
}

function noxss($s){
    return htmlspecialchars($s, ENT_QUOTES, 'UTF-8');
}

function get_upload_folder(){
    return "./secure__upload__folder/";
}
?>
