<?php

// FLAG 1: APRK{-WHAT_AN_LF!-}

$blacklist = ["compress.zlib","php://filter/read","php://filter/zlib","php://filter/convert.base64","base64","crypt","printable","compress","string"];

$b1 = strtolower($_SERVER['HTTP_ACCEPT_LANGUAGE']);
$b2 = strtolower(urldecode($_SERVER['HTTP_ACCEPT_LANGUAGE']));

foreach($blacklist as $b) {
    if ((stripos($b1,$b) !== false) || (stripos($b2,$b) !== false)){
        exit("WAF Protection Enabled !<br>
        [DEBUG] alert due to <b>".htmlspecialchars($b, ENT_QUOTES, 'UTF-8')."</b>");
    }
}
?>
