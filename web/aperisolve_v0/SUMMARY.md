# Aperi'Solve v0

Un membre de l'équipe Aperi'Kube a développé une plateforme de stéganographie. Ce dernier l'utilise régulièrement pour superviser les uploads d'images et voler des flags ! À votre tour de voler le flag en accédant à l'accès administrateur.

<u>**Note&nbsp;:**</u> La plateforme originale est disponible à l'adresse [https://aperisolve.fr](https://aperisolve.fr), cette dernière n'est pas vulnérable. Il est interdit d'attaquer la plateforme originale.

<u>URI&nbsp;:</u> [https://aperisolve-v0.aperictf.fr](https://aperisolve-v0.aperictf.fr)

