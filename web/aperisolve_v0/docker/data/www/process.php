<?php

$MaxFileSize = 999999999999999999;//500000;
$extArray = ["jpg","jpeg","jfif","jpe","png","bmp","gif","tif","tiff"];
$maxWidth = 999999999999999999;//500;
$maxHeight = 999999999999999999;//300;

function generateRandomString($length = 16) {
    // https://stackoverflow.com/questions/4356289/php-random-string-generator
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

// option -a pour zsteg
$zstegall = (int)@$_POST['incheckzsteg'];
if ($zstegall !== 1){
    $zstegall = 0;
}

// on Submit
$uploadOkFinal = 0;
$uploadOk = 1;
// try{
    $uploaddir = __DIR__.'/uploads/'; // can be /var/www/uploads
    $uploaddir2 = __DIR__.'/uploads_bot/'; // can be /var/www/uploads
    
    $imageFileType = strtolower(pathinfo(basename($_FILES['fileup']['name']),PATHINFO_EXTENSION));
    // $uploadfile = $uploaddir.basename($_FILES['fileup']['name']);
    $newname = generateRandomString();
    $uploadfile = $uploaddir.$newname.".".$imageFileType;
    $uploadfile2 = $uploaddir2.$newname.".".$imageFileType;
    
    // Check: Fausse Image
    $check = getimagesize($_FILES["fileup"]["tmp_name"]);
    if ($check === false) { // Pas une image
        $uploadOk = 0;
    }
    // Check: Fichier existe déjà
    if (file_exists($uploadfile)) { 
        $uploadOk = 0;
    }
	
	// echo($uploadOk);
    // Check: Taille trop grande
    if ($_FILES["fileup"]["size"] > $MaxFileSize){
        $uploadOk = 0;
    }
    // Check: Image ext
    if (!in_array(strtolower($imageFileType), $extArray)){
        $uploadOk = 0;
    }
    // Upload
    if ($uploadOk == 1){
        if (move_uploaded_file($_FILES["fileup"]["tmp_name"], $uploadfile)) {
            copy($uploadfile,$uploadfile2);
            $uploadOkFinal = 1;
        } else {
            $uploadOk = 0;
        }
    }
// }catch (Exception $e) { $uploadOk = 0; }
if ($uploadOkFinal == 1){
    echo($newname.".OK"); // On retourne le fichier de logs et on exec
    // Fichier = $uploadfile;
    
    echo(shell_exec("python process.py ".$uploadfile." ".$zstegall." 2>&1")); // Bloquant
    // fichier process.py: supprimer les images vieilles d'il y a > 1 jour ?
}else{
    echo("0");
}
?>
