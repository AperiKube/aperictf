#!/usr/bin/python
# -*- encoding:utf-8 -*-

import sys
import os
import time
from subprocess import PIPE, Popen
from PIL import Image
import base64

if len(sys.argv) != 3:
    sys.exit()

maxTime = 2*60 # en secondes 
folderup = "./uploads/"

myImg = Image.open(sys.argv[1])
uploadFormat = myImg.format



forceformat = False # ZSteg: on converti en png si non PNG / BMP
if myImg.mode == "1" or (uploadFormat != "PNG" and uploadFormat != "BMP"):
    uploadFormat = "PNG"
    forceformat = ".png"


# On récupère le fichier / l'extension
nomFichier = os.path.basename(sys.argv[1])
if "." in nomFichier:
    nomFichierNoExt = os.path.splitext(nomFichier)[0]
    fichierExt = os.path.splitext(nomFichier)[1]
else:
    nomFichierNoExt = nomFichier
    fichierExt = ".png"
if forceformat:
    fichierExt = forceformat
    myImg.save(folderup+nomFichierNoExt+fichierExt)



def cmdline(command):
    process = Popen(
        args=command,
        stdout=PIPE,
        shell=True
    )
    return process.communicate()[0]

# On recup option -a
if sys.argv[2] == "1":
    zstegOut = cmdline("zsteg "+str(folderup+nomFichierNoExt+fichierExt)+" --all")
else:    
    zstegOut = cmdline("zsteg "+str(folderup+nomFichierNoExt+fichierExt))

# ExifTool
exifOut = cmdline("exiftool -E -a -u -g1 "+str(folderup+nomFichierNoExt+fichierExt))
exifOut = exifOut.split("\n")
newlist = []
for l in exifOut:
    if "File Name" not in l and "./uploads" not in l and "File Permissions" not in l:
        newlist.append(l)
exifOut = '\n'.join(newlist)

if myImg.mode == "P" or myImg.mode == "1":
    myImg = myImg.convert('RGBA')

nbCanaux = len(myImg.split())

# sys.exit()
# On extrait les différents canaux

C1,C2,C3,C4 = False,False,False,False
if nbCanaux == 1:
    C1 = myImg.split()
elif nbCanaux == 2:
    C1,C2 = myImg.split()
elif nbCanaux == 3:
    C1,C2,C3 = myImg.split()
elif nbCanaux == 4:
    C1,C2,C3,C4 = myImg.split()

# On Recup les Img en LSB

full = [ Image.eval(myImg, lambda px: int(bin(px)[2:].zfill(8)[i])*255) for i in range(7,-1,-1)]

def get8imgBits(C):
    """ Renvoi les 8 images MSB à LSB """
    return [Image.eval(C, lambda a: int(bin(a)[2:].zfill(8)[i])*255) for i in range(7,-1,-1)]

imgsC1 = get8imgBits(C1)
imgsC2,imgsC3,imgsC4 = [],[],[]
if nbCanaux >= 2:
    imgsC2 = get8imgBits(C2)
if nbCanaux >= 3:
    imgsC3 = get8imgBits(C3)
if nbCanaux >= 4:
    imgsC4 = get8imgBits(C4)

couches = [imgsC1,imgsC2,imgsC3,imgsC4]

exifOut = exifOut.replace("&", "&amp;").replace('"', "&quot;").replace("<", "&lt;").replace(">", "&gt;")
zstegOut = zstegOut#.replace("&", "&amp;").replace('"', "&quot;").replace("<", "&lt;").replace(">", "&gt;")



with open(folderup+nomFichierNoExt+".OK","a+") as f:
    f.write(base64.b64encode(exifOut))
    f.write("*")
    f.write(base64.b64encode(zstegOut))
    f.write("*")
    f.write(str(nbCanaux)+"\r\n")
    i = 1
    for l in full:
        l.save(folderup+nomFichierNoExt+"_ALL_bit_n_"+str(i)+fichierExt,uploadFormat)
        f.write(folderup+nomFichierNoExt+"_ALL_bit_n_"+str(i)+fichierExt+"\r\n")
        i += 1
    for j in range(len(couches)):
        i = 1
        for l in couches[j]:
            l.save(folderup+nomFichierNoExt+"_C"+str(j)+"_bit_n_"+str(i)+fichierExt,uploadFormat)
            f.write(folderup+nomFichierNoExt+"_C"+str(j)+"_bit_n_"+str(i)+fichierExt+"\r\n")
            i += 1



# On supprime les vieux fichiers
now = time.time()
for f in os.listdir(folderup):
    fi = os.path.abspath(folderup)+"/"+f
    if os.stat(fi).st_mtime < now - maxTime:
        if os.path.isfile(fi) and "index.php" not in fi:
            os.remove(fi)


