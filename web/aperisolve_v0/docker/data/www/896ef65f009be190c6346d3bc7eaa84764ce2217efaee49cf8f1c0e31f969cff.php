<?php
$flag = "APRK{We1rdXSSV3ct0r}";
if (strpos(gethostbyaddr($_SERVER['REMOTE_ADDR']),"aperisolve-selenium") !== false){
	$dir = 'uploads_bot/';
	$files = Null;
	foreach (glob($dir.'*.png') as $filename) {
	  $time = filemtime($filename);
	  $files[$time] = $filename;
	}

	if ($files !== Null){
		krsort($files);
		$filenameOK = str_replace("_bot","",str_replace(".png",".OK",reset($files)));
	}//	var_dump($files);	var_dump($filenameOK);
?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta charset="UTF-8">
        <title>Aperi'Solve</title>
        <!-- Meta -->
        <meta name="Content-Language" content="en">
        <meta name="Keywords" content="AperiSolve, Zeecka">
        <meta name="Author" content="Alex GARRIDO">
        <meta name="Revisit-After" content="1 day">
        <meta name="Robots" content="all">
        <meta name="Distribution" content="global">
        <meta name="theme-color" content="#42f4c5">
        <link href="https://fonts.googleapis.com/css?family=Questrial|Righteous" rel="stylesheet"> 
        <link rel="stylesheet" href="style.css"/>
        <link rel="shortcut icon" type="image/x-icon" href="watermelon.png" />
        <link rel="icon" type="image/png" href="watermelon.png" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="main.js"></script>
    </head>
    <body>
        <h1 id="h1titre"><a href="#" id="homelink"><img src="watermelon.png" style="vertical-align: middle; width: 95px; height: 95px; margin-right: 10px;"/> Aperi'Solve</a></h1>
        <hr id="hrtitre"/>
        <section>
            <div id="info">
            <h2>Flag !</h2>
            <p>Congratz flag is: <b><?php echo($flag); ?></b></p>
            </div><!--
            --><div id="displayimg"><div id="containerimg"></div></div>
        </section>
    <script>$(document).ready(function(){

    function escapeHtml(text) {
        return text
        .replace(/&/g, "&amp;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;")
        .replace(/"/g, "&quot;")
        .replace(/'/g, "&#039;"); 
    }
    function askforfile(filename){
        if (filename == "0"){
            $("#txtbut").html("ERROR ! Reload page :/")
        }
	dragdropok = false;
	$.get(filename, function( data ) {
		    data = data.split("*");
		    imgsdata = data.pop();
		    data[0] = atob(data[0]);
		    data[0] = "<h2 class='h2info'>Exif</h2>"+escapeHtml(data[0]);
		    data[1] = atob(data[1]);
		    data[1] = "<h2 class='h2info'>Zsteg</h2>"+data[1];
		    data = data.join("");
		    data = data.replace('\r\n','\r');
		    data = data.split('\r');
		    data = data.filter(x=>x!=''); // remove empty string
		    data = data.filter(x=>x.slice(-3)!='.. '); // remove empty string
		    data = data.join('<br>');
		    data = data.split('\n');
		    data = data.join('<br>');
		    data = data.replace(/<br><br>/g,"<br>");
		    arrayOfLines = imgsdata.match(/[^\r\n]+/g);
		    nbCanaux = parseInt(arrayOfLines[0]);
            $("#info").slideUp();
            $("section").animate({width:"100%"});
            $('#containerimg').append("<h2 class='h2info'>View</h2>");
            if (nbCanaux >= 1){
                if(nbCanaux == 1){
                    $('#containerimg').append('<h2 class="h2scan">Grey scale</h2>');
                }else{
                    $('#containerimg').append('<h2 class="h2scan">Superimposed: </h2>');
                    for(i=1;i<9;i++){
                        $('#containerimg').append('<img class="imgscandisplay" src="'+arrayOfLines[i]+'" />');
                    }
                    $('#containerimg').append('<h2 class="h2scan" style="color:#f00">Red layers</h2>');
                }
                for(i=9;i<17;i++){
                    $('#containerimg').append('<img class="imgscandisplay" src="'+arrayOfLines[i]+'" />');
                }
            }
            if (nbCanaux >= 2){
                $('#containerimg').append('<h2 class="h2scan" style="color:#0f0">Green layers</h2>');
                for(i=17;i<25;i++){
                    $('#containerimg').append('<img class="imgscandisplay" src="'+arrayOfLines[i]+'" />');
                }
            }
            if (nbCanaux >= 3){
                $('#containerimg').append('<h2 class="h2scan" style="color:#00f">Blue layers</h2>');
                for(i=25;i<33;i++){
                    $('#containerimg').append('<img class="imgscandisplay" src="'+arrayOfLines[i]+'" />');
                }
            }
            if (nbCanaux >= 4){
                $('#containerimg').append('<h2 class="h2scan">Alpha layers</h2>');
                for(i=33;i<41;i++){
                    $('#containerimg').append('<img class="imgscandisplay" src="'+arrayOfLines[i]+'" />');
                }
            }
            
            
            $(".imgscandisplay").click(function(){
                newappend = '<div id="displayimgfull"><a href="'+$(this).attr("src")+'" download><img src="'+$(this).attr("src")+'" /></a></div>';
                $(newappend).hide().appendTo("body").fadeIn(300);

                $(document).keyup(function(e) {
                     if (e.keyCode == 27) { // echap
                        $("#displayimgfull").fadeOut(300, function() { $(this).remove(); });
                    }
                });
                $("#displayimgfull").click(function(){
                    $("#displayimgfull").fadeOut(300, function() { $(this).remove(); });
                }).children().click(function(e) {});
            });
            
            
            $('#containerimg').append("<br>");
            $('#containerimg').append(data);
		    
	});
}
askforfile("<?php echo($filenameOK) ?>");	
});</script>
    </body>
</html>
<?php }else{ ?>
<!doctype html>
<html lang="fr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta charset="UTF-8">
        <title>Aperi'Solve</title>
        <!-- Meta -->
        <meta name="Content-Language" content="en">
        <meta name="Keywords" content="AperiSolve, Zeecka">
        <meta name="Author" content="Alex GARRIDO">
        <meta name="Revisit-After" content="1 day">
        <meta name="Robots" content="all">
        <meta name="Distribution" content="global">
        <meta name="theme-color" content="#42f4c5">
        <link href="https://fonts.googleapis.com/css?family=Questrial|Righteous" rel="stylesheet"> 
        <link rel="stylesheet" href="style.css"/>
        <link rel="shortcut icon" type="image/x-icon" href="watermelon.png" />
        <link rel="icon" type="image/png" href="watermelon.png" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="main.js"></script>
    </head>
    <div style="visibility:hidden; opacity:0" id="dropzone"><div id="textnode">Drop anywhere!</div></div>
    <body>
        <h1 id="h1titre"><a href="#" id="homelink"><img src="watermelon.png" style="vertical-align: middle; width: 95px; height: 95px; margin-right: 10px;"/> Aperi'Solve</a></h1>
        <hr id="hrtitre"/>
        <section>
            <div id="info">
            <h2>Flag !</h2>
            <p>Congratz flag is: <b><?php echo($flag); ?></b></p>
            </div><!--
            --><div id="displayimg"><div id="containerimg"></div></div>
        </section>
    </body>
</html>
<?php } ?>
