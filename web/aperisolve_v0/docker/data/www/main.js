$(document).ready(function(){
    $("#checkzsteg").click(function(){
        $(this).toggleClass("active");
        $("#incheckzsteg").attr("value", (1+parseInt($("#incheckzsteg").attr("value")))%2);
    });

    function escapeHtml(text) {
        return text
        .replace(/&/g, "&amp;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;")
        .replace(/"/g, "&quot;")
        .replace(/'/g, "&#039;"); 
    }

    $("#filebut").click(function(){
        $("#fileup").click();
    });
    $("#fileup").change(function( event ){
        $("#txtbut").html(escapeHtml(this.value.replace(/^.*[\\\/]/, '')));
    });
    
    function progress(evt){
        if (evt.lengthComputable) {
            var percentComplete = evt.loaded / evt.total;
            //Do something with upload progress
            $("#progressbar").animate({ width: percentComplete*100+"%"},250,function(){
                if (percentComplete == 1){ // reset Progressbar
                    $("#progressbar").css({"opacity":"0"});
                    $("#progressbar").css({"width":"0%"});
                    $("#progressbar").css({"opacity":"1"});
                }
            });
        }
    }
    filename = null;
    $("#fileform").submit(function(e) {
        e.preventDefault();
        $("#backgroundprogressbar").show();
        $("#analyserbut").hide();
        $("#txtbut").html('File processing...<div id="loadinggif" class="lds-rolling"><div></div></div>');
        $("#filebut").css("cursor","auto");
        
        var formData = new FormData($("form").get(0));
        $.ajax({
            type : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url : 'process.php', // the url where we want to POST
            data : formData, // our data object
            dataType : 'json', // what type of data do we expect back from the server
            /*xhr: function() {
                var xhr = new window.XMLHttpRequest();
                // Upload progress
                xhr.upload.addEventListener("progress", function(evt){
                    progress(evt);
                }, false);
               return xhr;
            },*/
            processData: false,
            contentType: false,
            complete:function(data){
                filename = data.responseText;
                askforfile();
            }
        });
    });
    
    dragdropok = true;
    function askforfile(){
        if (filename == "0"){
            $("#txtbut").html("ERROR ! Reload page :/")
        }
        dragdropok = false;
        $.get( "uploads/"+filename, function( data ) {
            data = data.split("*");
            imgsdata = data.pop();
            data[0] = atob(data[0]);
            data[0] = "<h2 class='h2info'>Exif</h2>"+escapeHtml(data[0]);
            data[1] = atob(data[1]);
            data[1] = "<h2 class='h2info'>Zsteg</h2>"+data[1];
            data = data.join("");
            data = data.replace('\r\n','\r');
            data = data.split('\r');
            data = data.filter(x=>x!=''); // remove empty string
            data = data.filter(x=>x.slice(-3)!='.. '); // remove empty string
            data = data.join('<br>');
            data = data.split('\n');
            data = data.join('<br>');
            data = data.replace(/<br><br>/g,"<br>");
            arrayOfLines = imgsdata.match(/[^\r\n]+/g);
            nbCanaux = parseInt(arrayOfLines[0]);
            
            $("#info").slideUp();
            $("section").animate({width:"100%"});
            $('#containerimg').append("<h2 class='h2info'>View</h2>");
            if (nbCanaux >= 1){
                if(nbCanaux == 1){
                    $('#containerimg').append('<h2 class="h2scan">Grey scale</h2>');
                }else{
                    $('#containerimg').append('<h2 class="h2scan">Superimposed: </h2>');
                    for(i=1;i<9;i++){
                        $('#containerimg').append('<img class="imgscandisplay" src="'+arrayOfLines[i]+'" />');
                    }
                    $('#containerimg').append('<h2 class="h2scan" style="color:#f00">Red layers</h2>');
                }
                for(i=9;i<17;i++){
                    $('#containerimg').append('<img class="imgscandisplay" src="'+arrayOfLines[i]+'" />');
                }
            }
            if (nbCanaux >= 2){
                $('#containerimg').append('<h2 class="h2scan" style="color:#0f0">Green layers</h2>');
                for(i=17;i<25;i++){
                    $('#containerimg').append('<img class="imgscandisplay" src="'+arrayOfLines[i]+'" />');
                }
            }
            if (nbCanaux >= 3){
                $('#containerimg').append('<h2 class="h2scan" style="color:#00f">Blue layers</h2>');
                for(i=25;i<33;i++){
                    $('#containerimg').append('<img class="imgscandisplay" src="'+arrayOfLines[i]+'" />');
                }
            }
            if (nbCanaux >= 4){
                $('#containerimg').append('<h2 class="h2scan">Alpha layers</h2>');
                for(i=33;i<41;i++){
                    $('#containerimg').append('<img class="imgscandisplay" src="'+arrayOfLines[i]+'" />');
                }
            }
            
            
            $(".imgscandisplay").click(function(){
                newappend = '<div id="displayimgfull"><a href="'+$(this).attr("src")+'" download><img src="'+$(this).attr("src")+'" /></a></div>';
                $(newappend).hide().appendTo("body").fadeIn(300);

                $(document).keyup(function(e) {
                     if (e.keyCode == 27) { // echap
                        $("#displayimgfull").fadeOut(300, function() { $(this).remove(); });
                    }
                });
                $("#displayimgfull").click(function(){
                    $("#displayimgfull").fadeOut(300, function() { $(this).remove(); });
                }).children().click(function(e) {});
            });
            
            
            $('#containerimg').append("<br>");
            $('#containerimg').append(data);
            
        });
        
        
        
    }
    
    
    // Drag & Drop support, thx internet @bryc https://stackoverflow.com/questions/28226021/entire-page-as-a-dropzone-for-drag-and-drop
    
    var lastTarget = null;

    function isFile(evt) {
        var dt = evt.dataTransfer;

        for (var i = 0; i < dt.types.length; i++) {
            if (dt.types[i] === "Files") {
                return true;
            }
        }
        return false;
    }

    window.addEventListener("dragenter", function (e) {
        if (isFile(e) && dragdropok) {
            lastTarget = e.target;
            document.querySelector("#dropzone").style.visibility = "";
            document.querySelector("#dropzone").style.opacity = 1;
            document.querySelector("#textnode").style.fontSize = "48px";
        }
    });

    window.addEventListener("dragleave", function (e) {
        e.preventDefault();
        if ((e.target === document || e.target === lastTarget) && dragdropok) {
            document.querySelector("#dropzone").style.visibility = "hidden";
            document.querySelector("#dropzone").style.opacity = 0;
            document.querySelector("#textnode").style.fontSize = "42px";
        }
    });

    window.addEventListener("dragover", function (e) {
        e.preventDefault();
    });

    window.addEventListener("drop", function (e) {
        e.preventDefault();
        document.querySelector("#dropzone").style.visibility = "hidden";
        document.querySelector("#dropzone").style.opacity = 0;
        document.querySelector("#textnode").style.fontSize = "42px";
        if(e.dataTransfer.files.length == 1 && dragdropok)
        {
            let fileInput = document.querySelector('#fileup');
            fileInput.files = e.dataTransfer.files;
            document.querySelector("#txtbut").innerHTML = escapeHtml(e.dataTransfer.files[0].name);
        }
    });
    
    
    
});
