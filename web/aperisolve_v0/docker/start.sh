#!/bin/bash

chmod 777 data/www/uploads data/www/uploads_bot

docker-compose pull
docker-compose build
docker-compose push
docker-compose up -d

source .env
screen -dmS ${COMPOSE_PROJECT_NAME} docker-compose logs -f

