package com.example.demo.controller

import freemarker.template.Configuration
import freemarker.template.Template
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import java.io.IOException
import freemarker.template.TemplateException
import org.springframework.web.bind.annotation.*
import java.util.HashMap
import java.io.StringWriter
import java.io.StringReader


@Controller
class HelloController {
    @RequestMapping("/HjRn9hbrrKbE44CS")
    @ResponseBody
    fun home(@RequestBody(required = false) data: String?): String {
        System.out.println("Connected : " + data);
        var data = data
        if (data == null) {
            data = ""
        }
        var template = "<h3>OK</h3>" +
                "<p>"+data+"</p>"


        //dependent of the template engine
        //https://freemarker.apache.org/docs/api/freemarker/cache/StringTemplateLoader.html
        try {
            val t = Template("home", StringReader(template), Configuration())
            val out = StringWriter()
            try {
                t.process(HashMap<Any, Any>(), out)
            } catch (e: TemplateException) {
                e.printStackTrace()
            }

            template = out.toString()
        } catch (e: IOException) {

            e.printStackTrace()
        }

        System.out.println("Out:" + template)
        return template
    }
}