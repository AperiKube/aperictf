# JS Art

Vous auditez le site web d'une galerie d'*art digital*. L'administrateur entrepose ses dernières oeuvres (flag) dans un fichier/dossier caché aux utilisateurs. Investiguez et ramenez la dernière oeuvre.

<u>**Note&nbsp;:**</u> Inutile d'avoir un shell sur la machine.

<u>URI&nbsp;:</u> [https://jsart.aperictf.fr](https://jsart.aperictf.fr)
