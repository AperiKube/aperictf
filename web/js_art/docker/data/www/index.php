<?php

$default_color = "dark";

if (isset($_GET['dark'])){
    setcookie("color","dark");
    header("Location: /");
    exit();
}
if (isset($_GET['light'])){
    setcookie("color","light");
    header("Location: /");
    exit();
}
?>
<!doctype html>
<html lang="fr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta charset="UTF-8">
        <title>JS Art</title>
        <!-- Meta -->
        <meta name="Content-Language" content="en">
        <meta name="Keywords" content="AperiSolve, Zeecka">
        <meta name="Author" content="Alex GARRIDO">
        <meta name="Robots" content="all">
        <meta name="Distribution" content="global">
        <meta name="theme-color" content="#fae596">
        <link href="https://fonts.googleapis.com/css?family=DM+Serif+Text|Open+Sans&display=swap" rel="stylesheet"> 
        <link href="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/3.0.5/fullpage.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="main.js"></script>
        <script src="fullpage.js"></script>
        <link href="style.css" rel="stylesheet"><!-- http://lifeinsys.com/item/malena/dark/index.html#home -->
        <link href="fonts/font-awesome.min.css" rel="stylesheet">
    </head>
    <body>
    <?php
        if (empty($_COOKIE["color"])){
            @setcookie("color",$default_color);
            @$_COOKIE["color"] = $default_color;
        }
        $xmlDoc = new DOMDocument();
        $xmlDoc->load("arts.xml");
        $xslDoc = new DOMDocument();
        $url = $_COOKIE["color"].".xsl";
        $xslDoc->load($url);

        $proc = new XSLTProcessor();
        $proc->registerPHPFunctions();
        $proc->importStyleSheet($xslDoc);
        $out = $proc->transformToXML($xmlDoc);
        echo($out);
        
        //include("php://filter/convert.base64-encode/resource=main.js");
    ?>
        <script src="mainend.js"></script>
    </body>
</html>

