tnmt = [{'name': 'Leonardo', 'desc': 'The tactical, courageous leader and devoted student of his sensei.'},
    {'name': 'Michelangelo', 'desc' : 'The most stereotypical teenager of the team, he is a free-spirited, relaxed, goofy and jokester.'},
    {'name': 'Donatello', 'desc' : 'The scientist, inventor, engineer, and technological genius.'},
    {'name': 'Raphael', 'desc' : 'The team\'s bad boy, Raphael wears a red mask and wields a pair of sai.'},
    {'name': 'Splinter', 'desc' : 'The Turtles\' sensei and adoptive father, Splinter is a Japanese mutant rat.'}
]

s = new URL(window.location.href).searchParams.get('s');

if (s !== null) {
    desc = '';

    for(var i=0; i<tnmt.length; i++) {
        if(s.includes(tnmt[i]['name'])) {
            desc = tnmt[i].desc;
            break;
        }
    }

    if (desc !== '') {
        tmp = document.createElement('template'); // Used to filter HTML / JS
        tmp.innerHTML = s;
        document.getElementById('data').innerHTML = tmp.innerHTML + ' : ' + desc;
    } else {
        document.getElementById('data').innerHTML = 'No result';
    }
} else {
    document.getElementById('suggestion').outerHTML = '';
}

