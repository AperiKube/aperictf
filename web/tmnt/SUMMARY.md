# TMNT Search

Splinter commence à perdre la mémoire, il a donc développé un site web lui permettant de retrouver les informations sur les différents membres de son équipe.
D'après lui son site ne contient aucune vulnérabilité, car tout est géré "côté client". Prouvez-lui le contraire.

<u>**Note&nbsp;:**</u> Faites appel à un membre du staff afin de valider le challenge, l'exécution d'une XSS avec la fonction `alert();` est suffisante.

<u>URI&nbsp;:</u> [https://tmnt.aperictf.fr](https://tmnt.aperictf.fr)

