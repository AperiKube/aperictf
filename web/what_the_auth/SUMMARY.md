# What the auth

Une société a mis au point une nouvelle méthode d'authentification et a inscrit sa solution à un programme de bug bounty. Cassez l'authentification et ramenez leur le contenu de leur base de données.

<u>URI&nbsp;:</u> [https://what.aperictf.fr](https://what.aperictf.fr)
