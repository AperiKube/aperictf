import random
from PIL import Image

def genImage(text,nom):
    img = Image.new('RGB', (200, 200))
    pxlist = []
    j = 0
    for i in range(200*200):
        if j < len(text):
            r = ord(text[j])
        else:
            r = random.randint(0,255)
        j += 1
        if j < len(text):
            g = ord(text[j])
        else:
            g = random.randint(0,255)
        j += 1
        if j < len(text):
            b = ord(text[j])
        else:
            b = random.randint(0,255)
        j += 1
        px = (r,g,b)
        pxlist.append(px)
    img.putdata(pxlist)
    img.save(nom)
