/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : localhost:3306
 Source Schema         : mydb

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 12/05/2019 18:20:13
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for accounts
-- ----------------------------
DROP TABLE IF EXISTS `accounts`;
CREATE TABLE `accounts`  (
  `user` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `passwd` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL
) ENGINE = MyISAM CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of accounts
-- ----------------------------
INSERT INTO `accounts` VALUES ('guest', 'guest', 'Guest account');
INSERT INTO `accounts` VALUES ('flavien', 'Flag_is_APRK{StegQL_InJeCt10n_pVKzJ4pSP4EM93a5sR326QJtUnfVaxWC}', 'Administrator');
INSERT INTO `accounts` VALUES ('jean', '8E4aYMuk8nF', 'Jean');
INSERT INTO `accounts` VALUES ('leo', 'yAaXBY7dQ5s', 'Léo');
INSERT INTO `accounts` VALUES ('samuel', 'GqY8TSg9SeA', 'Samu, developpeur');
INSERT INTO `accounts` VALUES ('kevin', 'vY7qbKRzd5CXBH3KntkkR5n', 'Kevin');
INSERT INTO `accounts` VALUES ('bastien', 'fE5rqwTPL5w', 'Bastien');

SET FOREIGN_KEY_CHECKS = 1;


COMMIT;

GRANT SELECT on s3cr3ts_us3rs.* TO 'user' IDENTIFIED BY '2cWL2auRtd33pRBuFAjDqTq3MGsUEHgZ';
COMMIT;
