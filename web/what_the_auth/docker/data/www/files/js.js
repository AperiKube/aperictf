$( document ).ready(function() {
    $("#filebutton").click(function(){
        $("#file").click();
    });
    
    $('#file').change(function(e){
        var fileName = e.target.files[0].name;
        $("#filebutton").text(fileName);
    });
    
    $("#submitbutton").click(function(){
        var formData = new FormData($('form').get(0)); // get the form data
        $.ajax({
            type:'POST',
            url:'login.php',
            data:formData,
            processData: false,
            contentType: false,
            success:function(d){
                if (d == "redirect"){
                    $("#form").fadeOut(function(){
                        document.location = "/user.php";
                    });
                }else{
                    $("#error").text(d);
                    $("#error").show();
                    console.dir(d);
                }
            }
        });
    });
    $("#exit").click(function(){
        $("h1").fadeOut();
        $("a").fadeOut(function(){
            document.location = "/exit.php";
        });
    });
    $("form").fadeIn();
    $("h1").fadeIn();
    $("a").fadeIn();
});
