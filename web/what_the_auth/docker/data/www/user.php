<?php
session_start();
if (@$_SESSION['logged'] !== True){  // Redirect if not logged
	session_destroy();
	header("Location: /");
	exit();
}else{
?>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8" />
	<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet"> 
    <link rel="stylesheet" href="files/style.css"/>
	<link rel="shortcut icon" href="files/logo.png">
    <title>What The Auth</title>
</head>
<body>
	<h1>Welcome <?= htmlspecialchars(@$_SESSION['user'], ENT_QUOTES, 'UTF-8'); ?> !</h1>
	<a id="exit"></a>
</body>
<script src="files/jquery-3.4.0.min.js"></script>
<script src="files/js.js"></script>
</body>
</html>
<?php } ?>