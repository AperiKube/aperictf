#!/usr/bin/env python3

import sys
assert sys.version_info[0] == 3  # ==> Run in Python3 only ;)

input = r"Cipher"
output = r"Cipher.png"

r = [0,0,0,0,0,1,0,0,0,0,1,0,0,1,0,0] # registres


with open(input,"rb") as f:
    binary = ''.join([bin(l)[2:].zfill(8) for l in f.read()])

outbin = ''
for b in binary:
    new = r[0] ^ int(not (int(not r[7]) ^ ( r[13] ^ r[15] )))  # Next step
    outbin += str(int(b) ^ r[-1])  # Xor with output bit
    r = [new]+r[:-1] # Update registers


out = bytes([int(outbin[z:z+8],2) for z in range(0,len(outbin),8)])

with open(output,"wb") as f:
    f.write(out)
