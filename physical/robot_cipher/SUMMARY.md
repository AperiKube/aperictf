# Robot Cipher

L'entreprise "MetalHead" utilise une solution de chiffrement hardware "Next Gen".
Vous avez été mandaté pour vérifier la robustesse de cette solution.

<u>**Fichiers&nbsp;:**</u>
- [Robot.png](files/Robot.png) - md5sum: 9be15749b7c8276113452dbefd4a154a
- [Cipher](files/Cipher) - md5sum: e44b002660a92efd5fe555b0dc7999ce