# Fire Ducky
L'utilisateur du PC ducky semble avoir une activité suspecte sur son navigateur. Investiguez.
L'exfiltration des données peut se faire à l'aide d'une clé USB classique (montage D:// par default) uniquement.
Le flag respecte le format APRK{...}.

## Rubber Ducky - Fonctionnement

Pendant la durée du CTF, vous disposerez d'une clé USB HID "Rubber Ducky" de la société Hak5 prêtée par la société APIXIT.
Ce type de dispositif est une clé programmable qui, une fois branchée, agit comme un clavier USB (HID).
Vous utiliserez donc le langage "Ducky Script" proposé par Hak5 pour programmer votre clé USB et résoudre chaque challenge.

Si vous débutez avec les Rubber Ducky, nous vous conseillons d'utiliser le site [https://ducktoolkit.com/encode](https://ducktoolkit.com/encode) pour générer vos payloads en prenant soin de sélectionner la langue "FR".
Une vidéo d'explication est également disponible ici: https://youtu.be/kag1VsM-tzA
Documentation du langage: https://www.hak5.org/gear/duck/ducky-script-usb-rubber-ducky-101
Si vous avez des questions, n'hésitez pas à contacter le Staff.

## Environnement

Chaque challenge sera testé sur le PC "Ducky" situé au fond de la salle.
Ce PC tourne sous Windows 7 pro [EN] et possède les caractéristiques suivantes:
- Powershell 2.0/CMD fonctionnel
- Python 3.7 + module requests installé (Dans le PATH, accessible via `python`)
- CURL installé (Dans le PATH, accessible via `curl`)
- 7z installé (Dans le PATH, accessible via `7z` en cmd et ``` `7z` ``` sous powershell)

<u>**Note&nbsp;:**</u> 
- Les bind/reverse-shell et dropper sont interdit ! La payload ne doit être stoquée que sur la clé Rubber Ducky !
- Pas de triche ! Déposez le script non compilé (duckycode.txt) sur la clé. Aperi'Kube s'autorise une supervision/inspection sur la machine et les clés.
- Le matériel Rubber Ducky (clé Rubber Ducky + Adaptateur + Carte micro SD) vous est prété par la société APIXIT.
Le matériel doit être retourné par chaque équipe en fin de CTF. Le non retour du matériel entraînera une disqualification de l'équipe.
- Vous développerez et TESTEREZ VOTRE EXPLOIT EN LOCAL avant de demander à un membre du Staff de tester votre clé sur le PC "Ducky". Le nombre de tests est illimité.
