# Golden Key

Vous êtes missionné pour réaliser un test d'intrusion physique au sein de l'entreprise Reynholm Industries.

Au cours de votre mission, vous avez trouvé un PC verrouillé sur lequel est branché un équipement de stockage
de mots de passe *Mooltipass Mini*.

Investiguez et récupérez le compte Administrateur du PC&nbsp;!

<u>**Note&nbsp;:**</u> 
- Aucun guessing&nbsp;!
- Il s'agit d'un challenge **réaliste en deux parties**;
- L'usage de la force n'est pas nécessaire et interdit sur ce challenge&nbsp;;
- Prière de ne pas démonter les équipements&nbsp;!
