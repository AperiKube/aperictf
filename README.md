# Aperi'CTF

## Diagram

![network diagram](infra/doc/aperinet.png)

## Service list

| Category    | Frontend                                                            | Backend port |
|-------------|---------------------------------------------------------------------|--------------|
| Pwn         | [Pwn, Run, See](tcp://pwn-run-see.aperictf.fr:31337)                  | `31337/tcp`  |
| Pwn         | [Crazy Machine](tcp://crazy.aperictf.fr:31338)                        | `31338/tcp`  |
| Pwn         | [Java Jail](ssh://chall:V5fCqvaGULh7XGK3@javajail.aperictf.fr:31339)  | `31339/tcp`  |
| Pwn         | [Bin army 1](ssh://chall:7heqDtFq6b83qx9s@binarmy1.aperictf.fr:31340) | `31340/tcp`  |
| Pwn         | [Backdoor](ssh://chall:WRcPLQHRxoX6GWS7@backdoor.aperictf.fr:31341)   | `31341/tcp`  |
| Crypto      | [Secure Remote Password](tcp://srp.aperictf.fr:9898)                  | `9898/tcp`   |
| Crypto      | [Black Box](tcp://black-box.aperictf.fr:9897)                         | `9897/tcp`   |
| Misc        | [x32 Intro](tcp://x32.aperictf.fr:32321)                              | `32321/tcp`  |
| Misc        | [x32 Advanced](tcp://x32.aperictf.fr:32322)                           | `32322/tcp`  |
| Misc        | [x32 Emulator](tcp://x32.aperictf.fr:32323)                           | `32323/tcp`  |
| Misc        | [x32 Assembler](tcp://x32.aperictf.fr:32324)                          | `32324/tcp`  |
| Misc        | [Porte Blindée](tcp://porte.aperictf.fr:32325)                        | `32325/tcp`  |
| Misc        | [Emoji](https://emoji.aperictf.fr)                                    | `32326/tcp`  |
| Pentest     | [Not Humans](tcp://humans.aperictf.fr:33331)                          | `33331/tcp`  |
| Web         | [What the auth](https://what.aperictf.fr)                             | `10001/tcp`  |
| Web         | [Sometimes](https://sometimes.aperictf.fr)                            | `10002/tcp`  |
| Web         | [Hell no php](https://hell-no-php.aperictf.fr)                        | `10003/tcp`  |
| Web         | [Aperisolve v0](https://aperisolve-v0.aperictf.fr)                    | `10004/tcp`  |
| Web         | [TMNT](https://tmnt.aperictf.fr)                                      | `10005/tcp`  |
| Android/Web | [AndroLogger](https://andrologger.aperictf.fr/)                       | `10006/tcp`  |
| Web         | [JsArt](https://jsart.aperictf.fr)                                    | `10007/tcp`  |
| Web         | [Js Injection](https://jsinject.aperictf.fr)                          | `10008/tcp`  |
| Web         | [Oracle](https://oracle.aperictf.fr)                                  | `10009/tcp`  |
| Web         | [WorldMeet](https://worldmeet.aperictf.fr)                            | `10010/tcp`  |
