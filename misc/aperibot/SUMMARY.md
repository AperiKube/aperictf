# Aperi'Bot - Bienvenue

Bonjour et bienvenue à l'Aperi'CTF!

Rendez-vous sur Telegram pour jouer avec notre bot [@APRK_bot](https://telegram.me/APRK_bot).

Pour vous authentifier, envoyez cette commande au bot&nbsp;:

```
/auth zjny65fbxBmz2Q4G9e4vBQKT5fWuTnSUbW9PPgKa6hTuzpVPzjUa7FvsN5VAkMey
```

<u>**Note&nbsp;:**</u> Si vous ne possédez pas de compte sur Telegram et ne souhaitez pas en créer un, contactez le staff.
