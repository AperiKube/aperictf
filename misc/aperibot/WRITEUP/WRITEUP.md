+++
title = "Aperi'Bot - Bienvenue"
description = "Aperi'CTF 2019 - MISC (50 pts)"
keywords = "MISC, Telegram, Bot, Rock Paper Scissors, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = "2019-09-14T13:00:00+02:00"
weight = 20
draft = false
bref = "Aperi'CTF 2019 - MISC (50 pts)"
toc = true
+++

Aperi'CTF 2019 - Aperi'Bot - Bienvenue
============================================

### Challenge details

| Event                    | Challenge             | Category      | Points | Solves |
|--------------------------|-----------------------|---------------|--------|--------|
| Aperi'CTF 2019           | Aperi'Bot - Bienvenue | MISC          |  50    |  ???   |

Welcome to the Aperi'CTF!
Check out Telegram and play with our [@APRK_bot](https://telegram.me/APRK_bot) bot.
To authenticate yourself, send this command to the bot:
`/auth zjny65fbxBmz2Q4G9e4vBQKT5fWuTnSUbW9PPgKa6hTuzpVPzjUa7FvsN5VAkMey`
Note: If you don't have a Telegram account and don't want to create one, you can ask the staff for the flag.

### TL;DR
To get the flag, we had to win a game called `Rock, Paper, Scissors, Lizard, Spock` on Telegram.

### Methodology

Aperi'Bot is the welcome challenge of the Aperi'CTF event. It has been built to provide a simple test for Internet access, ensure that all teams are familiar with the platform and be fun 😊

Task description:

 > Welcome to the Aperi'CTF!
 >
 > Check out Telegram and play with our [@APRK_bot](https://telegram.me/APRK_bot) bot.
 >
 > To authenticate yourself, send this command to the bot:
 >
 > `/auth zjny65fbxBmz2Q4G9e4vBQKT5fWuTnSUbW9PPgKa6hTuzpVPzjUa7FvsN5VAkMey`
 >
 > Note: If you don't have a Telegram account and don't want to create one, you can ask the staff for the flag.

Let the challenge begin!

#### Telegram bot

First, we need to checkout the `APRK_bot` using our Telegram account:

![APRK_bot checkout](img/aprk_bot_checkout.png)

Let's start a new conversation with the bot:

![APRK_bot start](img/aprk_bot_start.png)

We're given a set of command list that we can use to interact with the bot, let's send ❤ to Aperi'Kube!

![APRK_bot love](img/aprk_bot_love.png)

Okay, the bot seems to work properly, we should now be able to authenticate and get the flag!

![APRK_bot flag](img/aprk_bot_flag.png)

The bot wants to play a game, let's play!

#### THE GAME

The game that the bot wants to play with us is the Rock, Paper, Scissors, Lizard, Spock which is a variant of the famous
Rock, Paper, Scissors with the following rule:

 * Scissors cuts Paper
 * Paper covers Rock
 * Rock crushes Lizard
 * Lizard poisons Spock
 * Spock smashes Scissors
 * Scissors decapitates Lizard
 * Lizard eats Paper
 * Paper disproves Spock
 * Spock vaporizes Rock
 * Rock crushes Scissors

After several attempts to beat the bot, we finally get the flag:

![APRK_bot rpsis](img/aprk_bot_rpsls.png)

#### Flag

The final flag is `APRK{W3lc0m3_t0_Ap3r1_CTF!!}`

*Happy Hacking!*

*Creased*
