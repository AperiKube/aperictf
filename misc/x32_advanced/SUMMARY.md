# x32 Advanced

Maintenant que vous vous êtes fait la main, prouvez que vous êtes à la hauteur de votre mission. Votre tâche est d'écrire un programme qui récupérera 30 caractères de STDIN et appliquera l'opération XOR avec la clé "X" avant d'afficher le résultat sur STDOUT.

```bash
nc x32.aperictf.fr 32322
```
