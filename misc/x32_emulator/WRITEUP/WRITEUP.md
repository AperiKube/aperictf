# x32 Emulator Writeup

The service gives us x32 bytecode encoded in hexadecimal and require us to supply the output of executing the bytecode. If our given result doesn't match the expected one, the server will gracefully show us the intended result. 300 checks must be passed and each time a new bytecode is generated, we can't fool the server by remembering the expected outputs. We have to implement an emulator.

To simplify a bit this tedious task, we know that the bytecode produced by the service is 100% valid, so there is no need to spend much time on error handling.

## General idea

First, it's mandatory to read the documentation of the language because it's what we will need to implement and there are examples given. To implement an emulator, I decided to write a python class.

An x32 emulator must have a stack, registers, stdin, stdout and a set of instructions to execute.

### The stack

To represent the stack a simple list of 256 bytes is used.

### Registers

Registers are properties of the class, one per register. A table is used to map register IDs to register names.
Functions to get/set a register value based on it's name must be implemented.

### IO

STDIN and STDOUT are both represented by a byte string. This way we can dissociate between the real IO streams of the machine running the emulator and directly access the result by accessing a property.

### Instructions

A table is used to map each bytecode to an instruction and a corresponding function that will handle the logic behind this instruction.

### Execution flow

To execute the entire bytecode, the emulator will start at IP=0. Depending on the bytecode encountered the appropriate function will be called. Each instruction updates the IP register which will automatically move to the next instruction.

## Solution

The complete class can be found in [emulator.py](exploit/emulator.py).

We can now use it to dialogue with the service and pass all the checks[(solve.py)](exploit/solve.py).

```
python3 solve.py
Test 1/300 : SUCCESS
Test 2/300 : SUCCESS
Test 3/300 : SUCCESS
...
Test 298/300 : SUCCESS
Test 299/300 : SUCCESS
Test 300/300 : SUCCESS
You passed all the tests, here you go :
APRK{Th4ts_S0m3_c00l_3mul4t10n_y0u_G0t_Th3r3!}
```

ENOENT
