#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# Author: ENOENT

from assembler import Assembler
from emulator import Emulator
import sys
import binascii
import string, random

FLAG = "APRK{Th4ts_S0m3_c00l_3mul4t10n_y0u_G0t_Th3r3!}"

def genlabel():
    return ''.join(random.choice(string.ascii_uppercase+string.ascii_lowercase+string.digits) for _ in range(random.randint(7, 9)))

def m1(char, i):
    regs = ["R1", "R2", "R3", "R4"]
    r1 = random.choice(regs)
    regs.pop(regs.index(r1))
    r2 = random.choice(regs)
# SET R1, 0x41
# SET R2, SP
# ADD R2, i
# STORE R2, R1
    return "SET {2}, {0}\nSET {3}, SP\nADD {3}, {1}\nSTORE {3}, {2}\n".format(hex(ord(char)), hex(i), r1, r2)

def m2(char, i):
    regs = ["R1", "R2", "R3", "R4"]
    r1 = random.choice(regs)
# set R1, SP
# ADD SP, 1+i
# push 0x41
# SET SP, R1
    return "SET {2}, SP\nADD SP, {1}\nPUSH {0}\nSET SP, {2}\n".format(hex(ord(char)), hex(i+1), r1)

def m3(char, i):
    regs = ["R1", "R2", "R3", "R4"]
    r1 = random.choice(regs)
    regs.pop(regs.index(r1))
    r2 = random.choice(regs)
    label = genlabel()
# SET R1, 0x41
# XOR R1, R1
# JG label
# XOR R1, 0x41
# <label>
# set R2, SP
# ADD SP, 1+i
# push R1
# SET SP, R2
    return "SET {2}, {0}\nXOR {2}, {2}\nJG {4}\nXOR {2}, {0}\n<{4}>\nSET {3}, SP\nADD SP, {1}\nPUSH {2}\nSET SP, {3}\n".format(hex(ord(char)), hex(i+1), r1, r2, label)

METHODS = [m1, m2, m3]

def generateLetter(i):
    char = random.choice(string.ascii_uppercase+string.ascii_lowercase+string.digits)
    return random.choice(METHODS)(char, i)

def generate():
    asm = Assembler()
    size = random.randint(1, 0xFF)

    code = "SUB SP, 0xFF\n"
    for i in range(size):
        code += generateLetter(i)
    code += "SET A1, {}\n".format(hex(size))
    code += "OUT SP"
    byteCode = asm.compile(code)
    print("Byte code in hex : {}".format(binascii.hexlify(byteCode).decode('utf-8')))
    return byteCode

def runTest():
    try:
        byteCode = generate()
        emu = Emulator(byteCode)
        emu.run()
        expected = emu.STDOUT
        print("What is the outut of this programm ?")
        given = sys.stdin.buffer.read(len(expected))
        if given == expected:
            return True
        else:
            print("Your input : {}\nExpected result : {}".format(given, expected))
    except Exception as inst:
        x, y = inst.args
        print("Server generated faulty x32 code.\nThe point is for you I guess :)")
        print("{} : {}".format(x,y))
        return True

if __name__ == "__main__":
    N = 300
    giveflag = True
    for i in range(N):
        r = runTest()
        if r:
            print("Test {}/{} : SUCCESS".format(i+1,N))
        else:
            print("Test {}/{} : FAILED".format(i+1,N))
            giveflag = False
            break
    if giveflag:
        print("You passed all the tests, here you go :")
        print(FLAG)
