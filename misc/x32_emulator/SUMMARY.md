# x32 Emulator

Maintenant que vous vous êtes suffisamment familiarisés avec ce langage, il va vous falloir un moyen d'exécuter du bytecode x32. Comment comptez-vous analyser leurs microcontrôleurs sinon ?

Le serveur va vous donner plusieurs suites de bytecode x32, il faudra lui envoyer le résultat de l'exécution de celles-ci. Vous pouvez être sûr que le serveur ne vous enverra que du bytecode valide.

```bash
nc x32.aperictf.fr 32323
```
