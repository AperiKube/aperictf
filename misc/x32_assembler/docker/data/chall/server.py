#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# Author: ENOENT

from assembler import Assembler
import random, sys
import string

FLAG = "APRK{1_g0t_Th3_p0w3r_t0_c0mp1l3_4nyth1ng_N0w_g1mm3_th3_M0N3Y!!}"
bytecode = bytes()

def genlabel():
    return ''.join(random.choice(string.ascii_uppercase+string.ascii_lowercase+string.digits) for _ in range(random.randint(7, 9)))

def generateCode():
    code = []
    regs = ["SP", "R1", "R2", "R3", "R4", "A1"]
    consts = [hex(i) for i in range(0, 128, 7)]
    insts = ["IN", "OUT", "PUSH"]
    dinsts = ["SET", "LOAD", "CMP", "ADD", "SUB", "XOR"]
    jumps = ["goto", "je", "jg", "jl"]
    for i in range(random.randint(10, 50)):
        if random.randint(0, 25) == 10:
            # put a label
            label = genlabel()
            code.append("<{}>".format(label))
            code.append(" {} {}".format(random.choice(jumps), label))
        elif random.randint(0, 2) == 1:
            # two argument instruction
            code.append("{} {}, {}".format(random.choice(dinsts), random.choice(regs), random.choice(regs+consts)))
        elif random.randint(0, 2) == 1:
            # add a comment
            code.append("# {}".format(random.choice(consts+regs+jumps+dinsts+insts)))
        elif random.randint(0, 2) == 1:
            # add a blank line
            code.append("")
        else:
            # single argument instruction
            code.append("{}{} {}".format("\t"*random.randint(0, 4), random.choice(insts), random.choice(regs+consts)))
    random.shuffle(code)
    return "\n".join(code)

def runTest():
    asm = Assembler()
    bytecode = bytes()
    try:
        code = generateCode()
        bytecode = asm.compile(code)
        print("---- START ----")
        print(code)
        print("---- END ----")
        user = getUserByteCode(len(bytecode))
        if bytecode != user:
            print("Your byte code : {}\nExpected bytecode : {}".format(user, bytecode))
        else:
            return True
    except Exception as inst:
        x, y = inst.args
        print("Server generated faulty x32 code.\nThe point is for you I guess :)")
        print("{} : {}".format(x,y))
        return True


def getUserByteCode(expectedLen):
    print("What length is your byte code ?")
    length = sys.stdin.readline().strip()
    try:
        length = int(length)
    except:
        print("Please enter a valid length.")
        sys.exit()
    if length != expectedLen:
        print("That's not good, don't bother sending me your garbage.")
        sys.exit()
    print("Give me your byte code :")
    return sys.stdin.buffer.read(length)

if __name__ == "__main__":
    N = 1000
    giveflag = True
    for i in range(N):
        r = runTest()
        if r:
            print("Test {}/{} : SUCCESS".format(i+1,N))
        else:
            print("Test {}/{} : FAILED".format(i+1,N))
            giveflag = False
            break
    if giveflag:
        print("You passed all the tests, here you go :")
        print(FLAG)
