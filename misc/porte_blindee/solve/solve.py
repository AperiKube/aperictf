#!/usr/bin/env python3
# -*- coding:utf-8 -*-
import math
import numpy as np
from pwn import *

LHOST = 'porte.aperictf.fr'
LPORT = 32325

r = remote(LHOST, LPORT)

def orientation(p, q, r):
    """
    Renvoie l'orientation d'un triplet ordonné (p, q, r). 
    @param p: Point 1
    @param q: Point 2
    @param r: Point 3
    @return: L'orientations du triplet
            0 --> p, q and r sont alignés 
            1 --> Sens horraire 
            2 --> Sens anti-horraire
    """
    val = ((q[1] - p[1]) * (r[0] - q[0])) - \
          ((q[0] - p[0]) * (r[1] - q[1])) 
  
    if (val == 0):
        return 0  # Allignés 
    return 1 if (val > 0) else 2 # Sens horraire / anti-horraire


def convexHull(pts):
    """
    Calcul la liste de points composant le "convex hull" 
    (polygone de taille minimum comprenant tous les points). 
    @param pts: liste de points d'entrée
    @return: list de points composant le convex hull.
    """
    hull = []
    n = len(pts)
        
    if (n < 3):  # 3 points minimums
        return None
    
    l = 0
    for i in range(1, n):  # Recherche du points le plus à gauche
        if pts[i][0] < pts[l][0]:
            l = i
    
    p = None
    while (p != l):  # tant qu'on revient pas au début
        if p is None:  # fix le premier point
            p = l
    
        hull.append(pts[p]) # On ajoute le point au convex hull
        
        q = (p+1)%n  # On cherche q, point suivant le + antihorraire
        for i in range(n):
           if (orientation(pts[p], pts[i], pts[q]) == 2):
               q = i
        
        p = q

    return hull

def minBoundingRect(convex_hull):
    """ 
    Utilise numpy pour convertir un convex hull vers un minimum bounding rectangle
    On utilise ici la méthode dite de "rottating callipers".
    On cherche à minimiser ou maximiser (@mini) une @condition qui peut soit être 
    une aire ("area"), soit une largeur ("width")
    Inspiration: https://gist.github.com/kchr/77a0ee945e581df7ed25 
    """
    
    # Calcule des distances (x2-x1,y2-y1)
    dist = np.zeros((len(convex_hull)-1,2))
    for i in range(len(dist)):
        dist_x = convex_hull[i+1,0] - convex_hull[i,0]
        dist_y = convex_hull[i+1,1] - convex_hull[i,1]
        dist[i] = [dist_x,dist_y]

    angles = np.zeros((len(dist)))  # On calcule les angles atan2(y/x)
    for i in range(len(angles)):
        angles[i] = math.atan2(dist[i,1], dist[i,0])

    for i in range(len(angles)):  # cast positif
        angles[i] = abs(angles[i] % (math.pi/2))

    # On retire les doublons
    angles = np.unique(angles)

    # On vérifie chaque "box" pour optimiser le critère choisi
    calc_box = (0, float("inf"), 0, 0, 0, 0, 0, 0) # rot_angle, aire, largeur, hauteur, min_x, max_x, min_y, max_y
    for i in range(len(angles)):
        # On créer une matrice de rotation pour le convex_hull
        # R = [cos(theta), cos(theta-PI/2), cos(theta+PI/2), cos(theta)]
        R = np.array([ [ math.cos(angles[i]), math.cos(angles[i]-(math.pi/2)) ], [ math.cos(angles[i]+(math.pi/2)), math.cos(angles[i]) ] ])
        # On transpose le convex_hull
        rot_points = np.dot(R, np.transpose(convex_hull))

        # On récupère les angles de notre rectangle
        min_x = np.nanmin(rot_points[0], axis=0)
        max_x = np.nanmax(rot_points[0], axis=0)
        min_y = np.nanmin(rot_points[1], axis=0)
        max_y = np.nanmax(rot_points[1], axis=0)

        # On calcule la hauteur,largeur et l'air du rectangle
        width = max_x - min_x
        height = max_y - min_y
        area = width*height

        # On optimise
        if (area < calc_box[1] or i == 0):
            calc_box = ( angles[i], area, width, height, min_x, max_x, min_y, max_y )

    # On récupère les angles non transposés
    min_x = calc_box[4]
    max_x = calc_box[5]
    min_y = calc_box[6]
    max_y = calc_box[7]
    
    # On recalcule la matrice de rotation
    angle = calc_box[0]   
    R = np.array([ [ math.cos(angle), math.cos(angle-(math.pi/2)) ], [ math.cos(angle+(math.pi/2)), math.cos(angle) ] ])

    # On retranspose pour avoir les vrais points
    points = np.zeros( (4,2) ) # empty 2 column array
    points[0] = np.dot( [ max_x, min_y ], R )
    points[1] = np.dot( [ min_x, min_y ], R )
    points[2] = np.dot( [ min_x, max_y ], R )
    points[3] = np.dot( [ max_x, max_y ], R )

    return (calc_box[1], calc_box[2], calc_box[3], points) # area, width, height, points

for i in range(100):
    rec =  r.recvuntil("plaque:\n")
    print(rec)
    entree = eval(rec.split("Impacts: ")[1].split("\n")[0])
    print(entree)
    ch = convexHull(entree)  # Convex Hull
    ch = [ch[-1]]+ch  # fix for MBR
    mbr = minBoundingRect(np.array(ch))  # MBR
    mbr = str([(int(round(x[0])),int(round(x[1]))) for x in mbr[-1]])[1:-1]  # On cast en tuple
    print(mbr)
    r.sendline(mbr)
r.interactive()
