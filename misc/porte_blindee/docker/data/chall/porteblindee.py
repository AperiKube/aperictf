#!/usr/bin/env python3

import math
import numpy as np
import socket
import random
import sys
from secret import FLAG

def orientation(p, q, r):
    """
    Renvoie l'orientation d'un triplet ordonne (p, q, r). 
    @param p: Point 1
    @param q: Point 2
    @param r: Point 3
    @return: L'orientations du triplet
            0 --> p, q and r sont alignes 
            1 --> Sens horraire 
            2 --> Sens anti-horraire
    """
    val = ((q[1] - p[1]) * (r[0] - q[0])) - \
          ((q[0] - p[0]) * (r[1] - q[1])) 
  
    if (val == 0):
        return 0  # Allignes 
    return 1 if (val > 0) else 2 # Sens horraire / anti-horraire


def convexHull(pts):
    """
    Calcul la liste de points composant le "convex hull" 
    (polygone de taille minimum comprenant tous les points). 
    @param pts: liste de points d'entre
    @return: list de points composant le convex hull.
    """
    hull = []
    n = len(pts)
        
    if (n < 3):  # 3 points minimums
        return None
    
    l = 0
    for i in range(1, n):  # Recherche du points le plus a gauche
        if pts[i][0] < pts[l][0]:
            l = i
    
    p = None
    while (p != l):  # tant qu'on revient pas au debut
        if p is None:  # fix le premier point
            p = l
    
        hull.append(pts[p]) # On ajoute le point au convex hull
        
        q = (p+1)%n  # On cherche q, point suivant le + antihorraire
        for i in range(n):
           if (orientation(pts[p], pts[i], pts[q]) == 2):
               q = i
        
        p = q

    return hull

def minBoundingRect(convex_hull, condition="area", mini=True):
    """ 
    Utilise numpy pour convertir un convex hull vers un minimum bounding rectangle
    On utilise ici la methode dite de "rottating callipers".
    On cherche a minimiser ou maximiser (@mini) une @condition qui peut soit etre 
    une aire ("area"), soit une largeur ("width").
    L'algo renvoie une liste de box.
    Inspiration: https://gist.github.com/kchr/77a0ee945e581df7ed25 
    """
    
    # Calcule des distances (x2-x1, y2-y1)
    dist = np.zeros((len(convex_hull)-1, 2))
    for i in range(len(dist)):
        dist_x = convex_hull[i+1,0] - convex_hull[i,0]
        dist_y = convex_hull[i+1,1] - convex_hull[i,1]
        dist[i] = [dist_x, dist_y]

    angles = np.zeros((len(dist)))  # On calcule les angles atan2(y/x)
    for i in range(len(angles)):
        angles[i] = math.atan2(dist[i,1], dist[i,0])

    for i in range(len(angles)):  # cast positif
        angles[i] = abs(angles[i] % (math.pi/2))

    # On retire les doublons
    angles = np.unique(angles)

    # On verifie chaque "box" pour optimiser le critere choisi
    calc_box = [(0, float("inf"), 0, 0, 0, 0, 0, 0)] # rot_angle, aire, largeur, hauteur, min_x, max_x, min_y, max_y
    for i in range(len(angles)):
        # On creer une matrice de rotation pour le convex_hull
        # R = [cos(theta), cos(theta-PI/2), cos(theta+PI/2), cos(theta)]
        R = np.array([ [ math.cos(angles[i]), math.cos(angles[i]-(math.pi/2)) ], [ math.cos(angles[i]+(math.pi/2)), math.cos(angles[i]) ] ])
        # On transpose le convex_hull
        rot_points = np.dot(R, np.transpose(convex_hull))

        # On recupere les angles de notre rectangle
        min_x = np.nanmin(rot_points[0], axis=0)
        max_x = np.nanmax(rot_points[0], axis=0)
        min_y = np.nanmin(rot_points[1], axis=0)
        max_y = np.nanmax(rot_points[1], axis=0)

        # On calcule la hauteur, largeur et l'air du rectangle
        width = max_x - min_x
        height = max_y - min_y
        area = width*height

        # On optimise la condition area ou width en la maximisant ou minimisant
        if mini:
            if condition == "area":
                if (area < calc_box[0][1] or i == 0):  # Si c'est mieux, on reinitialise la liste
                    calc_box = [( angles[i], area, width, height, min_x, max_x, min_y, max_y )]
                elif (area == calc_box[0][1]):  # Si c'est egal, on ajoute a la liste
                    calc_box.append((angles[i], area, width, height, min_x, max_x, min_y, max_y))
            elif condition == "width":
                if (min(width, height) < min(calc_box[0][2], calc_box[0][3]) or i == 0):
                    calc_box = [( angles[i], area, width, height, min_x, max_x, min_y, max_y )]
                elif (min(width, height) == min(calc_box[0][2], calc_box[0][3])):
                    calc_box.append((angles[i], area, width, height, min_x, max_x, min_y, max_y))
        else:
            if condition == "area":
                if (area > calc_box[0][1] or i == 0):
                    calc_box = [( angles[i], area, width, height, min_x, max_x, min_y, max_y )]
                elif (area == calc_box[0][1]):
                    calc_box.append((angles[i], area, width, height, min_x, max_x, min_y, max_y))
            elif condition == "width":
                if (max(width, height) > max(calc_box[0][2], calc_box[0][3]) or i == 0):
                    calc_box = [( angles[i], area, width, height, min_x, max_x, min_y, max_y )]
                elif (max(width, height) == max(calc_box[0][2], calc_box[0][3]) ):
                    calc_box.append((angles[i], area, width, height, min_x, max_x, min_y, max_y))

    output = []
    for c in calc_box:  # on renvoie chaque element de la liste
        # On recupere les angles non transposes
        min_x = c[4]
        max_x = c[5]
        min_y = c[6]
        max_y = c[7]
        
        # On recalcule la matrice de rotation
        angle = c[0]   
        R = np.array([[math.cos(angle), math.cos(angle-(math.pi/2))], [math.cos(angle+(math.pi/2)), math.cos(angle)]])

        # On retranspose pour avoir les vrais points
        points = np.zeros((4,2)) # empty 2 column array
        points[0] = np.dot([max_x, min_y], R)
        points[1] = np.dot([min_x, min_y], R)
        points[2] = np.dot([min_x, max_y], R)
        points[3] = np.dot([max_x, max_y], R)
        
        output.append((c[1], c[2], c[3], points)) # area, width, height, points
    return output

def isValid(mbr,inpuser):
    if len(inpuser) != 4:
        return False
    for sub in mbr:
        if len(set(sub).intersection(set(inpuser))) == 4:
            return True
    return False

def parsepoints(x):
    x = x.replace(" ","")
    x = x.replace("),(",");(")
    x = x.strip()
    x = x.split(";")
    if len(x) != 4:
        return None
    try:
        x = [[int(j) for j in l.replace("(","").replace(")","").split(",")] for l in x]
        x = [(j[0],j[1]) for j in x]
    except:
        return None
    return x

for i in range(1,101):  # 100 Etapes
    entree = [[random.randint(-i*100,i*100),random.randint(-i*100,i*100)] for x in range(i+4)]  # Trous aleatoires
    ch = convexHull(entree)  # Convex Hull
    mbr = minBoundingRect(np.array([ch[-1]]+ch),condition="area",mini=True)  # MBR
    mbr = [e[-1] for e in mbr]  # on recupere que les points du MBR
    mbr2 = [[(int(round(x[0])),int(round(x[1]))) for x in m ] for m in mbr]  # On cast en tuple
    
    ### Affichage de tour courrant
    sys.stdout.write("[Vague "+str(i)+"]\n")
    sys.stdout.write("*BANG ! BANG ! BANG*\n")
    sys.stdout.write("Impacts: "+str([(int(round(x[0])),int(round(x[1]))) for x in entree])+"\n")
    sys.stdout.write("\n")
    sys.stdout.write("Installation d'une nouvelle plaque:\n")
    x = sys.stdin.readline()
    x = parsepoints(x)  # Recup de l'entree utilisateur
    
    if x is None:
        sys.stdout.write("[ERREUR] Votre saisie est erronee !\n")
        sys.exit()
        break
    if not isValid(mbr2,x):
        sys.stdout.write("[ERREUR] Entree incorrecte ! Les valeurs attendues etaient les suivantes:\n")
        sys.stdout.write(str(mbr2[0]).replace("[","").replace("]",""))
        sys.exit()
        break
    
    sys.stdout.write("\n")
if i == 100:
    sys.stdout.write((FLAG+"\n"))

