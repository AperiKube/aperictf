# Porte Blindée

Lors d'une mission RED Team, la porte principale de l'entreprise Cybernaise s'est faite attaquer à coup de perceuse.
Les attaquants n'ont pas réussi à rentrer dans les locaux mais la porte est fortement endommagée et est donc vulnérable.
Afin de renforcer la porte en vue de futures intrusions, il est nécessaire d'établir un blindage sur la zone impactée.
La plaque de blindage étant très coûteuse, il est important de minimiser la quantité de matière et donc de minimiser la surface (aire) de cette plaque.

Votre mission est la suivante&nbsp;:

 - Déterminer les coordonnées des extrémités de la plaque de blindage de sorte à&nbsp;:
   - recouvrir tous les impacts de perceuse
   - minimiser la surface (aire) de cette plaque

Attention, vous devez survivre à plusieurs vagues d'intrusions de plus en plus puissantes.

Il est important d'indiquer les coordonnées de la plaque de blindage à chaque vague, indépendamment des vagues précédentes.

```bash
nc porte.aperictf.fr 32325
```

------------------------

<u>**Notes&nbsp;:**</u>

- La porte est ici un repère orthonormé où le point (0,0) correspond au centre de la porte
- Chaque trou effectué par la perceuse est modélisé par une coordonnée (x,y)
- La plaque de blindage est rectangulaire
- Vous devez renvoyer une liste de 4 coordonnées sous la forme "(x1,y1);(x2,y2);(x3,y3);(x4,y4)"
- Les coordonnées de la plaque doivent être des entiers (valeurs arrondies)
- L'ordre des coordonnées à envoyer n'a pas d'importance
- L'ordre et les coordonnées d'impacts reçus ne suivent pas une logique particulière
- Il y a au minimum 5 impactes par vague
- La notion d'épaisseur de foret n'est pas utile
- La notion d'échelle varie au fur et à mesure des vagues d'attaque, il n'est pas important de s'en soucier.
- Les impacts de perceuse sont compris sur `]-inf,+inf[`
- Il n'est pas nécessaire de prendre en compte les impacts des vagues précédentes
- 2 syntaxes de réponses sont disponibles, comme précisé dans l'exemple.

------------------------

<u>**Exemple de partie&nbsp;:**</u>

```
[Vague 1]
*BANG ! BANG ! BANG*
Impacts: [(-7, 2), (-7, 5), (-6, -2), (-2, 5), (1, 8), (2, -3), (-5, 2), (-5, 5), (-2, -1), (4, 1), (4, 5)]

Installation d'une nouvelle plaque.
>>>[(4, -3), (-7, -3), (-7, 8), (4, 8)]


[Vague 2]
*BANG ! BANG ! BANG*
Impacts: [(-4,2), (-12,4), (-7,-2), (4,5), (6,4), (5,5), (5,-5), (4,8)]

Installation d'une nouvelle plaque.
>>>(6,8);(5,-5);(-13,-3);(-11,10)

[Vague 3]
*BANG ! BANG ! BANG*
Impacts: [(0,3), (2,2), (1,6), (2,1), (3,0), (0,0), (3,3)]

Installation d'une nouvelle plaque.
>>>jkofdjsiodf
[ERREUR] Votre saisie est éronnée !
```
