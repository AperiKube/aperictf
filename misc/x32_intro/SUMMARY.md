# x32 Intro

La société ENO.corp fabrique des cartes à puce pour microcontrôleurs.
Ces cartes sont programmables grâce au langage d'assemblage inventé spécialement pour l'occasion: x32 !

Votre mission est de devenir un expert afin d'analyser les microcontrôleurs qu'ils vendent et les exploiter.

Afin de vous familiariser avec ce nouveau langage, votre tâche est d'écrire un mini-programme qui récupérera 10 caractères de STDIN et les affichera sur STDOUT.

```bash
nc x32.aperictf.fr 32321
```

<u>**Note&nbsp;:**</u> Lisez [la documentation](files/documentation.zip) de ce langage avant de commencer.
<u>**Ressource complémentaire&nbsp;:**</u> [la documentation](files/documentation.zip) - md5sum: 5ac1ef34b4641b319281e65d80e84411
