# x32 Intro Writeup

The following code is self explanatory. The only difficulty was thinking to save space on the stack before writing, otherwise an out of bounds exception occurs. The server is kind enough to give details on where a problem is located and the causes, should it be during assembling, running or testing.

```
---- START ----
set A1, 0xa     # We will receive and send 10 bytes
sub SP, 0xa     # Reserve enough space on the stack to hold our input
in SP           # Reads A1 bytes from STDIN and stores it in the stack at SP
out SP          # Writes A1 bytes from the stack at SP, to STDOUT
---- END ----
```

ENOENT
