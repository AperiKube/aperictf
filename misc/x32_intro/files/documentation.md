# x32 processor architecture

The x32 processors are running on a 8-bits, architecture much like a simplified version of x86.

## Components

### Registers

There are 4 general purpose registers named R1, R2, R3 and R4.

The SP register points towards the top of the stack.

The A1 register is used to pass arguments to specific instructions.

The IP register stores the address of the current instruction that is executed. This register cannot be modified directly.

### The code segment

The code segment is located in a separated memory location and can store up to 0xFFFF bytes.

The code starts at address 0x0.

### The stack

The stack starts at address 0xFF (value of SP at the beginning of the execution) and grows towards lower addresses. The stack is very limited in space and can only store 256 values.

![stack](images/x32_stack.png)

### The flags
There are 2 flags which can only be 0 or 1 :

- The zero flag (ZF) : Indicates that an operation resulted in a 0.
- The greater flag (GF) : Set by the *CMP* instruction to indicate that a value is greater than another.

## Instruction set

Assembly programs are made of individual instructions. Instructions generally take the form:

*opcode destination, source*

#### IN arg1

Reads the amount of bytes specified in A1 from STDIN and stores it on the stack at the address specified by arg1.

arg1 can be a register or an immediate value.

#### OUT arg1

Writes to STDOUT the amount of bytes specified in A1. The bytes are read from the stack at the address specified by arg1.

arg1 can be a register or an immediate value.

#### SET arg1, arg2

Places the value of arg2 in arg1.

arg1 must be a register.

arg2 can be a register or an immediate value.

#### LOAD arg1, arg2

Places the 8-bit value found in the stack at the address specified by arg2 in arg1.

arg1 must be a register.

arg2 can be a register or an immediate value.

#### STORE arg1, arg2

Places the 8-bit value of arg2 in the stack at the address specified by arg1.

arg1 can be a register or an immediate value.

arg2 must be a register.

#### ADD arg1, arg2

Performs the addition of the values specified by arg1 and arg2 and stores the resulting number in arg1.

arg1 must be a register.

arg2 can be a register or an immediate value.

ZF is modified by this instruction.

#### SUB arg1, arg2

Performs the subtraction of the value specified by arg2 to arg1 and stores the resulting number in arg1.

arg1 must be a register.

arg2 can be a register or an immediate value.

ZF is modified by this instruction.

#### XOR arg1, arg2

Performs the bitwase XOR of the values specified by arg1 and arg2 and stores the resulting number in arg1.

arg1 must be a register.

arg2 can be a register or an immediate value.

ZF is modified by this instruction.

#### PUSH arg1

First, SP is decremented by 1.

Pushes the value specified by arg1 to the stack at the addresse specified by SP.

arg1 can be a register or an immediate value.

#### POP arg1

Copies the 8-bits value from the top of the stack into arg1.

SP is incremented by 1 afterwards.

arg1 must be a register.

#### CMP arg1, arg2

Compares the values specified by arg1 and arg2.

If they are equal, the ZF flag is set.

If arg1 is greater than arg2, the GF flag is set.

arg1 must be a register.

arg2 can be a register or an immediate value.

ZF and GF are modified by this instruction.

#### JE arg1

If the ZF flag is set, jumps to the location of arg1.

arg1 must be a label.

#### JG arg1

If the GF flag is set, jumps to the location of arg1.

arg1 must be a label.

#### JL arg1

If the GF flag is not set and the ZF is not set either, jumps to the location of arg1.

arg1 must be a label.

#### GOTO arg1

Jumps to the location of arg1.

arg1 must be a label.

## Preprocessing

The only recognized formats for opcode's arguments is register's names, immediate values or labels.

All immediate values must be in hexadecimal with the prefix "0x".

Labels must be defined on a single line surrounded by "<>", contain only characters in [a-zA-Z0-9_] and not correspond to any processor specific names.

Opcodes are not case sensitive.

Everything on the same line after the character "#" is considered a comment and is ignored.

Indentations, trailing spaces/tabs, and empty lines are ignored as well.

### Example

This is valid x32 code :

```
set A1, 0x1
sub SP, 0x2
cmp A1, 0x2
goto end
<start>
in SP			# Reads 1 byte from STDIN
load R2, SP		# loads 1 byte from the stack

set R1, 0x4
<label_1>		# for-loop that will add 0xa to the input
	add R2, R1
	sUb R1, 0x1
	cmp R1, 0x0
	JG label_1
<end>
jl start
store SP, R2
OUT SP			# print the result to STDOUT
```

Which would give :

```
STDIN : A
STDOUT : K

STDIN : h
STDOUT : r

STDIN : !
STDOUT : +
```

## Byte code conversion

### Byte code format

Each instruction in x32 is transformed into byte code by following this conversion table :

| bit 1 to 8 | bit 9                        | bit 10                       | bit 11 to 13 | bit 14 to 16 | bit 17 to 24 <br/>(optional) |
| ---------- | ---------------------------- | ---------------------------- | ------------ | ------------ | ---------------------------- |
| opcode id  | 1, if arg1<br/>is a register | 1, if arg2<br/>is a register | register id  | register id  | Immediate value              |

In case of jump instructions (opcode 0x6…), the bits 9 to 16 would never be used so this conversion table is used instead :

| first byte | Byte 2 and 3                                     |
| ---------- | ------------------------------------------------ |
| opcode id  | The address to which to jump in the code segment |

### Opcode ID

| opcode | ID   |
| ------ | ---- |
| IN     | 0x11 |
| OUT    | 0x12 |
| SET    | 0x31 |
| LOAD   | 0x22 |
| STORE  | 0x23 |
| ADD    | 0x32 |
| SUB    | 0x33 |
| XOR    | 0x34 |
| PUSH   | 0x41 |
| POP    | 0x42 |
| CMP    | 0x51 |
| JG     | 0x61 |
| JL     | 0x62 |
| JE     | 0x63 |
| GOTO   | 0x64 |

### Register ID

| Register | ID   |
| -------- | ---- |
| R1       | 0x1  |
| R2       | 0x2  |
| R3       | 0x3  |
| R4       | 0x4  |
| A1       | 0x5  |
| SP       | 0x6  |

### Examples

x32 assembly :

```
set R2, 0x87
```

Transforms into this byte code :

```
\x31\x90\x87
```



x32 assembly :

```
in SP
```

Transforms into this byte code :

```
\x11\xb0
```

The valid x32 code from the last section would transform into this byte code :

```
\x31\xa8\x01\x33\xb0\x02\x51\xa8\x02\x64\x00\x1e\x11\xb0\x22\xd6\x31\x88\x04\x32\xd1\x33\x88\x01\x51\x88\x00\x61\x00\x13\x62\x00\x0c\x23\xf2\x12\xb0
```
